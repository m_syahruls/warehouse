<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// list goodies
Route::get('goody/list', 'Api\GoodyController@list')->name('api.goody.list');
// checking category when make new goodies
Route::get('category/check/{id}', 'Api\GoodyController@checkCategory')->name('api.category.check');
// list categories
Route::get('category/list', 'Api\GoodyController@listCategory')->name('api.category.list');
Route::get('category/list/{id}', 'Api\GoodyController@listCategorySelected')->name('api.category.list.selected');
// list units
Route::get('unit/list', 'Api\GoodyController@listUnit')->name('api.unit.list');
Route::get('unit/list/{id}', 'Api\GoodyController@listUnitSelected')->name('api.unit.list.selected');
// list projects
Route::get('project/list', 'Api\StockOutController@listProject')->name('api.project.list');
// list for StockNow
Route::get('stocknow/list', 'Api\StockNowController@list')->name('api.stocknow.list');
// list for Transaction
Route::get('/transaction/permonth/{date}', 'Api\TransactionController@getPermonth')->name('api.transaction.permonth');
Route::get('/transaction/perproject/{project}', 'Api\TransactionController@getPerproject')->name('api.transaction.perproject');
// user list
Route::get('/transaction', 'Api\TransactionController@getUser')->name('api.transaction');
// transaction list per user
Route::get('/transaction/{id}', 'Api\TransactionController@getSingleUser')->name('api.transaction_single');