<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// landing page redirect to login
Route::get('/', function () {
    return Redirect::to('login');
})->name('landing');
// auth vanilla from laravel
Auth::routes();

// change language
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::group(['middleware' => 'auth'], function(){
    // diffrence between USER and ADMIN (Home View)
    // for admin
    Route::resource('/home', 'HomeController')->middleware('level'); 
    // for user
    Route::prefix('user')->name('user.')->group(function () {
        Route::resource('/home', 'User\HomeController');
    });
    // profile
    Route::resource('/profile', 'ProfileController');
    Route::get('/profile/{id}/password', 'ProfileController@editPassword');

    // Stock that can be access by User
    // Stock Now
    Route::get('/stocknow/export_pdf', 'StockNowController@exportPdf');
    Route::resource('/stocknow', 'StockNowController');
    // Export Stockout/Transaction
    Route::get('/stockout/export_pdf/{id}', 'StockOutController@exportSinglePdf')->name('stockout.export');
    // Transaction(request stockout)
    Route::get('/transaction/summary', 'TransactionController@summary');
    Route::get('/transaction/checkid', 'TransactionController@checkId')->name('transaction.checkid');
    Route::resource('/transaction', 'TransactionController');
    // cart
    Route::post('/cart/store', 'CartController@store')->name('cart.store');
    // Route::get('/cart/store_demo', 'CartController@storeDemo')->name('cart.store_demo'); // storing dummy goodies for development purpose
    Route::get('/cart/list', 'CartController@list')->name('cart.list');
    Route::get('/cart/clear', 'CartController@clear')->name('cart.clear');
    Route::get('/cart/{id}/stock', 'CartController@getStock')->name('cart.stock');
    Route::resource('/cart', 'CartController');
    // Stock Out
    Route::resource('/stockout/item', 'StockOutItemController',['as' => 'stockout']); // for Stockout items(edit/detail)

    Route::group(['middleware' => 'level'], function(){
        // for MASTER
        // category
        Route::post('/category/store_stockin', 'CategoryController@storeStockin')->name('category.store_stockin'); // store category from Stock In Page
        Route::resource('/category', 'CategoryController');
        // unit
        Route::post('/unit/store_stockin', 'UnitController@storeStockin')->name('unit.store_stockin'); // store unit from Stock In Page
        Route::resource('/unit', 'UnitController');
        // goody
        Route::post('/goody/store_stockin', 'GoodyController@storeStockin')->name('goody.store_stockin'); // store goody from Stock In Page
        Route::resource('/goody', 'GoodyController');
        // user
        Route::resource('/user', 'UserController');

        // ALL TRANSACTION(STOCK)
        // Stock Out
        Route::resource('/stockout', 'StockOutController');
        // Stock In
        Route::get('/stockin/summary', 'StockInController@summary');
        Route::get('/stockin/checkid', 'StockInController@checkId')->name('stockin.checkid');
        Route::get('/stockin/export_pdf/{id}', 'StockInController@exportSinglePdf');
        Route::resource('/stockin', 'StockInController');
        // Stock Opname
        Route::get('/stockopname/summary', 'StockOpnameController@summary');
        Route::get('/stockopname/checkid', 'StockOpnameController@checkId')->name('stockopname.checkid');
        Route::get('/stockopname/export_pdf/{id}', 'StockOpnameController@exportSinglePdf');
        Route::resource('/stockopname', 'StockOpnameController');
        // Setting
        Route::prefix('setting')->group(function () {
            Route::get('/', 'SettingController@index')->name('setting');
            Route::post('/building', 'SettingController@storeBuilding')->name('setting.building');
        });
    });
});
