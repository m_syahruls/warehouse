<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GoodySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('goodies')->insert([
            [
                'category_id' => "1",
                'code' => "AE 1",
                'name' => "Kipas Angin",
                'photo' => "kipas.jpg",
                'qty' => "1",
                'unit_id' => "1",
                'location' => "rak 1",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'category_id' => "1",
                'code' => "AE 2",
                'name' => "Air Conditioner",
                'photo' => "ac.jpg",
                'qty' => "1",
                'unit_id' => "1",
                'location' => "rak 2",
                'created_at' => Carbon::now()->addSeconds(30)->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->addSeconds(30)->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
