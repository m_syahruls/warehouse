<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => "Admin",
                'username' => "admin",
                'email' => "admin@synapsisproject.com",
                'password' => bcrypt('admin'),
                'level' => "admin",
                'photo' => "default.png",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => "User",
                'username' => "user",
                'email' => "user@synapsisproject.com",
                'password' => bcrypt('user'),
                'level' => "user",
                'photo' => "default.png",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
