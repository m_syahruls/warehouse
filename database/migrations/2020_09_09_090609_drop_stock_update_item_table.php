<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropStockUpdateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stock_update_item');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('stock_update_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('stock_id')->unsigned()->nullable();
            $table->bigInteger('goody_id')->unsigned()->nullable();
            $table->integer('qty');
            $table->enum('status', ['purchase', 'repair'])->nullable();
            $table->enum('condition', ['good', 'ok', 'bad'])->nullable();
            $table->timestamps();

            $table->foreign('stock_id')->references('id')->on('stock_update')->onDelete('cascade');
            $table->foreign('goody_id')->references('id')->on('goodies')->onDelete('cascade');
        });
    }
}
