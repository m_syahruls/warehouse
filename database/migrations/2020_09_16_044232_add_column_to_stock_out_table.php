<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToStockOutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_out', function (Blueprint $table) {
            $table->string('id_transaction')->unique()->after('id');
            $table->enum('status_transaction', ['unconfirmed', 'progress', 'done'])->nullable()->after('status')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_out', function (Blueprint $table) {
            $table->dropColumn('id_transaction');
            $table->dropColumn('status_transaction');
        });
    }
}
