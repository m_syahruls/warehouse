<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockOpnameItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opname_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('stock_id')->unsigned()->nullable();
            $table->bigInteger('goody_id')->unsigned()->nullable();
            $table->integer('qty');
            $table->enum('condition', ['good', 'ok', 'bad'])->nullable();
            $table->timestamps();

            $table->foreign('stock_id')->references('id')->on('stock_opname')->onDelete('cascade');
            $table->foreign('goody_id')->references('id')->on('goodies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_opname_item');
    }
}
