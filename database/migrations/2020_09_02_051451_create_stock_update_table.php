<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_update', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date');
            $table->string('description')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('stock_update', function(Blueprint $table){
        //     $table->dropForeign('stock_update_created_by_foreign');
        //     $table->dropColumn('created_by');
        // });
        Schema::dropIfExists('stock_update');
    }
}
