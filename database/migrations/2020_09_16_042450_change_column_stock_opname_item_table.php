<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnStockOpnameItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_opname_item', function (Blueprint $table) {
            $table->string('description')->nullable()->after('qty');
            $table->dropColumn('condition');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_opname_item', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->enum('condition', ['good', 'ok', 'bad'])->nullable()->after('qty');
        });
    }
}
