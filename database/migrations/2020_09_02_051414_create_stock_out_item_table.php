<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockOutItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_out_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('stock_id')->unsigned()->nullable();
            $table->bigInteger('goody_id')->unsigned()->nullable();
            $table->integer('qty');
            $table->timestamps();

            $table->foreign('stock_id')->references('id')->on('stock_out')->onDelete('cascade');
            $table->foreign('goody_id')->references('id')->on('goodies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('stock_out_item', function(Blueprint $table){
        //     $table->dropForeign('stock_out_item_stock_id_foreign');
        //     $table->dropColumn('stock_id');
        //     $table->dropForeign('stock_out_item_goody_id_foreign');
        //     $table->dropColumn('goody_id')
        // });
        Schema::dropIfExists('stock_out_item');
    }
}
