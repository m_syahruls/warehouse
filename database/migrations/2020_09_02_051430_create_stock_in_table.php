<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_in', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('out_id')->unsigned()->nullable();
            $table->enum('status', ['purchase', 'return'])->nullable();
            $table->dateTime('date');
            $table->string('description')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('out_id')->references('id')->on('stock_out')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('stock_in', function(Blueprint $table){
        //     $table->dropForeign('stock_in_out_id_foreign');
        //     $table->dropColumn('out_id');
        //     $table->dropForeign('stock_in_created_by_foreign');
        //     $table->dropColumn('created_by');
        // });
        Schema::dropIfExists('stock_in');
    }
}
