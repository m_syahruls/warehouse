<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStockoutItemIdToStockInItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_in_item', function (Blueprint $table) {
            $table->bigInteger('stockout_item_id')->unsigned()->nullable()->after('qty');
            $table->foreign('stockout_item_id')->references('id')->on('stock_out_item')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_in_item', function (Blueprint $table) {
            $table->dropForeign('stock_in_item_stockout_item_id_foreign');
            $table->dropColumn('stockout_item_id');
        });
    }
}
