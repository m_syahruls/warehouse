<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDateInStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_opname', function (Blueprint $table) {
            $table->dropColumn('date');
        });
        Schema::table('stock_opname', function (Blueprint $table) {
            $table->string('date')->after('name');
        });

        Schema::table('stock_out', function (Blueprint $table) {
            $table->dropColumn('date');
        });
        Schema::table('stock_out', function (Blueprint $table) {
            $table->string('date')->after('status_transaction');
        });

        Schema::table('stock_in', function (Blueprint $table) {
            $table->dropColumn('date');
        });
        Schema::table('stock_in', function (Blueprint $table) {
            $table->string('date')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_opname', function (Blueprint $table) {
            $table->dropColumn('date');
        });
        Schema::table('stock_opname', function (Blueprint $table) {
            $table->dateTime('date')->after('name');
        });

        Schema::table('stock_out', function (Blueprint $table) {
            $table->dropColumn('date');
        });
        Schema::table('stock_out', function (Blueprint $table) {
            $table->dateTime('date')->after('status_transaction');
        });

        Schema::table('stock_in', function (Blueprint $table) {
            $table->dropColumn('date');
        });
        Schema::table('stock_in', function (Blueprint $table) {
            $table->dateTime('date')->after('status');
        });
    }
}
