<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoodiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goodies', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->index()->unsigned()->nullable();
            $table->string('code')->unique();
            $table->string('name');
            $table->string('photo');
            $table->integer('qty');
            $table->bigInteger('unit_id')->unsigned()->nullable();
            $table->string('location')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('goods', function(Blueprint $table){
        //     $table->dropForeign('goods_category_id_foreign');
        //     $table->dropColumn('category_id');
        // });
        Schema::dropIfExists('goodies');
    }
}
