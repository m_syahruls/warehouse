<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // sidebar
    'home' => 'Home',
    'transaction' => 'Transaction',
    'stock' => 'Stock',
    'stocknow' => 'Stock Now',
    'stockin' => 'Stock In',
    'stockout' => 'Stock Out',
    'stockopname' => 'Stock Opname',
    'category' => 'Categories',
    'unit' => 'Units',
    'goodies' => 'Goodies',
    'user' => 'Users',
    'setting' => 'Settings',

    // navbar
    'login' => 'Login',
    'profile' => 'User Profile',
    'password' => 'Change Password',
    'logout' => 'Logout',
];
