<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Goodies Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master Goody',
    'title_create' => 'Create - Goody',
    'title_show' => 'Detail - Goody',
    'title_edit' => 'Edit - Goody',

    'button_create' => 'Create Goody',
    'button_update' => 'Update Goody',
    'title_delete' => 'Delete Goody',
    'subtitle_delete' => 'Are you sure you want to delete this Goody?',
];
