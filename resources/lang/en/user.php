<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master User',
    'title_create' => 'Create - User',
    'title_edit' => 'Edit - User',

    'button_create' => 'Create User',
    'button_update' => 'Update User',
    'title_delete' => 'Delete User',
    'subtitle_delete' => 'Are you sure you want to delete this User?',
];
