<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'title_login' => 'Log in to your account',
    'username' => 'Username',
    'password' => 'Password',
    'remember' => 'Remember Me',
    'button_login' => 'Login',
    'forgot' => 'Forgot Your Password?',

    'title_reset' => 'Reset Password',
    'email' => 'E-Mail Address',
    'button_reset' => 'Send Password Reset Link',
];
