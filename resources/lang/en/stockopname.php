<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Stock Opname Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title_create' => 'Create - Stock Opname',
    'title_summary' => 'Summary - Stock Opname',
    'title_detail' => 'Detail - Stock Opname',
    'title_delete' => 'Delete Stock Opname',
    'subtitle_delete' => 'Are you sure you want to delete this Stock Opname?',

    'button_create' => 'Record Stock Opname',
    'button_clear' => 'Clear Cart',
    'button_summary' => 'Summary',
];
