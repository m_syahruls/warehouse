<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Stock In Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title_create' => 'Create - Stock In',
    'title_summary' => 'Summary - Stock In',
    'title_detail' => 'Detail - Stock In',
    'title_delete' => 'Delete Stock In',
    'subtitle_delete' => 'Are you sure you want to delete this Stock In?',

    'button_create' => 'Record Stock In',
    'button_clear' => 'Clear Cart',
    'button_summary' => 'Summary',
];
