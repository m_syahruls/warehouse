<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Units Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master Unit',
    'title_create' => 'Create - Unit',
    'title_edit' => 'Edit - Unit',

    'button_create' => 'Create Unit',
    'button_update' => 'Update Unit',
    'title_delete' => 'Delete Unit',
    'subtitle_delete' => 'Are you sure you want to delete this Unit?',
];
