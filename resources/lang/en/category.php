<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Categories Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master Category',
    'title_create' => 'Create - Category',
    'title_edit' => 'Edit - Category',

    'button_create' => 'Create Category',
    'button_update' => 'Update Category',
    'title_delete' => 'Delete Category',
    'subtitle_delete' => 'Are you sure you want to delete this Category?',
];
