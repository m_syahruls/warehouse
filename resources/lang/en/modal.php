<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Modal Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // general
    'yes_delete' => 'Yes, Delete',
    'yes_remove' => 'Yes, Remove',

    // logout
    'title_logout' => 'End Current Session',
    'subtitle_logout' => 'Are you ready to end your current session?',

    // item
    'title_item' => 'Edit Item',

    // cart
    'title_add_cart' => 'Add to Cart',
    'title_edit_cart' => 'Edit Item in Cart',
    'title_delete_cart' => 'Remove from Cart',
    'subtitle_delete_cart' => 'Are you sure you want to remove this goody from Cart?',
    'over_cart_single' => "Goodies status is single can't be more than 1",
    'over_cart_multiple' => "Goodies can't be more than existing Stock",
    'zero_cart' => "Goodies Qty can't be 0",
];
