<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Transaction Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title_detail' => 'Detail - Stock Out',
    'title_delete' => 'Delete Stock Out',
    'subtitle_delete' => 'Are you sure you want to delete this Stock Out?',
];
