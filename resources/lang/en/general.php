<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'yes' => 'Yes',
    'no' => 'No',
    'add' => 'Add',
    'create' => 'Create',
    'save' => 'Simpan',
    'cancel' => 'Cancel',
    'back' => 'Back',
    'edit' => 'Edit',
    'update' => 'Update',
    'wait' => 'Wait...',
    'sending' => 'Sending...',
    'button_pdf' => 'Export PDF',
];
