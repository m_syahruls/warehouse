<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Profile Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'User Profile',
    'title_edit' => 'Edit - User Profile',
    'title_password' => 'Change Password - User Profile',

    'button_edit' => 'Edit Profile',
    'button_update' => 'Update Profile',
    'button_password' => 'Update Password',
];
