<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // general
    'empty' => 'Empty Data',

    // transactions
    'id' => 'Transaction ID',
    'date' => 'Date',
    'photo' => 'Photo',
    'created_by' => 'Created by',
    'updated_by' => 'Updated by',
    'updated_by_last' => 'Last Updated by',
    'description' => 'Description',
    'status' => 'Status',
    'action' => 'Action',

    // home
    'confirmation' => 'Confirmation',

    // stockin
    'id_stockout' => 'Stock Out ID',

    // opname
    'name_transaction' => 'Transaction Name',

    // goodies
    'name' => 'Name',
    'code' => 'Code',
    'category' => 'Category',
    'unit' => 'Unit',
    'qty' => 'Qty',
    'qty_request' => 'Qty Request',
    'qty_confirm' => 'Qty Confirm',
    'stock' => 'Stock',
    'location' => 'Location',

    // user
    'email' => 'Email',
    'username' => 'Username',
    'password' => 'Password',
    'password_confirm' => 'Confirm Password',
    'password_new' => 'New Password',
    'level' => 'Level',

    // building
    'address' => 'Address',
    'district' => 'District',
    'city' => 'City',
    'province' => 'Province',
    'postal' => 'Postal Code',
    'phone' => 'Phone',
    'website' => 'Website',
    'logo' => 'Logo',
];
