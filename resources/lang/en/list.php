<?php

return [

    /*
    |--------------------------------------------------------------------------
    | List Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cart' => 'Cart List',
    'stockout' => 'Stock Out List',
    'stocknow' => 'Stock Now List',
    'stockin' => 'Stock In List',
    'stockopname' => 'Stock Opname List',
    'goodies' => 'Goodies List',
];
