<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // general
    'title_detail' => 'Detail - Stock Out',
    'title_edit' => 'Edit - Stock Out',
    'title_delete' => 'Delete Stock Out',
    'subtitle_delete' => 'Are you sure you want to delete this Stock Out?',

    // admin
    'title_stockin' => 'Stock In',
    'subtitle_stockin' => 'Add new Stock In to add more goodies qty.',
    'title_stockout' => 'Stock Out',
    'subtitle_stockout' => ' Stock Out request that must be processed.',
    
    // user
    'title_transaction' => 'Transaction',
    'subtitle_transaction' => 'Request Transaction to get goodies.',
    'title_stocknow' => 'Stock Now',
    'subtitle_stocknow' => 'Check Goodies you need before make a new Transaction.',
];
