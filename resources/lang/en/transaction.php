<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Transaction',
    'title_create' => 'Create - Stock Out',
    'title_summary' => 'Summary - Stock Out',
    'title_detail' => 'Detail - Stock Out',

    'button_create' => 'Request Stock Out',
    'button_clear' => 'Clear Cart',
    'button_summary' => 'Summary',
];
