<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'yes' => 'Ya',
    'no' => 'Tidak',
    'add' => 'Tambah',
    'create' => 'Buat',
    'save' => 'Simpan',
    'cancel' => 'Batal',
    'back' => 'Kembali',
    'edit' => 'Ubah',
    'update' => 'Perbarui',
    'wait' => 'Tunggu...',
    'sending' => 'Mengirim...',
    'button_pdf' => 'Ekspor PDF',
];
