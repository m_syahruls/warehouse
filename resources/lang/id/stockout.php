<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Transaction Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title_detail' => 'Detail - Stok Keluar',
    'title_delete' => 'Hapus Stok Keluar',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Stok Keluar ini?',
];
