<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Goodies Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master Barang',
    'title_create' => 'Buat - Barang',
    'title_show' => 'Detail - Barang',
    'title_edit' => 'Ubah - Barang',

    'button_create' => 'Buat Barang',
    'button_update' => 'Perbarui Barang',
    'title_delete' => 'Hapus Barang',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Barang ini?',
];
