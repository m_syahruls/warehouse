<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Modal Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // general
    'yes_delete' => 'Ya, Hapus',
    'yes_remove' => 'Ya, Hapus',

    // logout
    'title_logout' => 'Akhiri sesi ini',
    'subtitle_logout' => 'Apakah anda ingin mengakhiri sesi ini?',

    // item
    'title_item' => 'Ubah Item',

    // cart
    'title_add_cart' => 'Tambah ke Keranjang',
    'title_edit_cart' => 'Ubah Barang di Keranjang',
    'title_delete_cart' => 'Hapus dari Keranjang',
    'subtitle_delete_cart' => 'Anda yakin ingin menghapus barang ini dari Keranjang?',
    'over_cart_single' => "Status Barang adalah satuan dan tidak bisa lebih dari 1",
    'over_cart_multiple' => "Barang tidak bisa lebih dari Stok yang ada",
    'zero_cart' => "Qty Barang tidak boleh 0",
];
