<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Transaction Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Transaksi',
    'title_create' => 'Buat - Stok Keluar',
    'title_summary' => 'Ringkasan - Stok Keluar',
    'title_detail' => 'Detail - Stok Keluar',

    'button_create' => 'Ajukan Stok Keluar',
    'button_clear' => 'Kosongkan Keranjang',
    'button_summary' => 'Ringkasan',
];
