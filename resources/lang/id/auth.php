<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'title_login' => 'Masuk menggunakan akun',
    'username' => 'Akun Pengguna(Username)',
    'password' => 'Kata Sandi',
    'remember' => 'Ingat Saya',
    'button_login' => 'Masuk',
    'forgot' => 'Lupa Kata Sandi Kamu?',

    'title_reset' => 'Reset Kata Sandi',
    'email' => 'Alamat E-mail',
    'button_reset' => 'Kirim Tautan',
];
