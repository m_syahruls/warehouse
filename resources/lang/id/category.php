<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Categories Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master Kategori',
    'title_create' => 'Buat - Kategori',
    'title_edit' => 'Ubah - Kategori',

    'button_create' => 'Buat Kategori',
    'button_update' => 'Perbarui Kategori',
    'title_delete' => 'Hapus Kategori',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Kategori ini?',
];
