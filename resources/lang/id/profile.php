<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Profile Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Profil Pengguna',
    'title_edit' => 'Ubah - Profil Pengguna',
    'title_password' => 'Ubah Kata Sandi - Profil Pengguna',

    'button_edit' => 'Ubah Profil',
    'button_update' => 'Perbarui Profil',
    'button_password' => 'Perbarui Kata Sandi',
];
