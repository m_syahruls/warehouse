<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // sidebar
    'home' => 'Beranda',
    'transaction' => 'Transaksi',
    'stock' => 'Stok',
    'stocknow' => 'Stok Sekarang',
    'stockin' => 'Stok Masuk',
    'stockout' => 'Stok Keluar',
    'stockopname' => 'Stok Opname',
    'category' => 'Kategori',
    'unit' => 'Satuan',
    'goodies' => 'Barang',
    'user' => 'Pengguna',
    'setting' => 'Pengaturan',

    // navbar
    'login' => 'Masuk',
    'profile' => 'Profil Pengguna',
    'password' => 'Ubah Kata Sandi',
    'logout' => 'Keluar',
];
