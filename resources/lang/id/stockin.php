<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Stock In Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title_create' => 'Buat - Stok Masuk',
    'title_summary' => 'Ringkasan - Stok Masuk',
    'title_detail' => 'Detail - Stok Masuk',
    'title_delete' => 'Hapus Stok Masuk',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Stok Masuk ini?',

    'button_create' => 'Catat Stok Masuk',
    'button_clear' => 'Kosongkan Keranjang',
    'button_summary' => 'Ringkasan',
];
