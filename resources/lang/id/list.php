<?php

return [

    /*
    |--------------------------------------------------------------------------
    | List Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cart' => 'Daftar Keranjang',
    'stockout' => 'Daftar Stok Keluar',
    'stocknow' => 'Daftar Stok Sekarang',
    'stockin' => 'Daftar Stok Masuk',
    'stockopname' => 'Daftar Stok Opname',
    'goodies' => 'Daftar Barang',
];
