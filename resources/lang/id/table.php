<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    // general
    'empty' => 'Data Kosong',

    // transactions
    'id' => 'ID Transaksi',
    'date' => 'Tanggal',
    'photo' => 'Foto',
    'created_by' => 'Dibuat oleh',
    'updated_by' => 'Diperbarui oleh',
    'updated_by_last' => 'Diperbarui oleh',
    'description' => 'Deskripsi',
    'status' => 'Status',
    'action' => 'Aksi',

    // home
    'confirmation' => 'Konfirmasi',

    // stockin
    'id_stockout' => 'ID Stok Keluar',

    // opname
    'name_transaction' => 'Nama Transaksi',

    // goodies
    'name' => 'Nama',
    'code' => 'Kode',
    'category' => 'Kategori',
    'unit' => 'Satuan',
    'qty' => 'Qty',
    'qty_request' => 'Qty Diminta',
    'qty_confirm' => 'Qty Disetujui',
    'stock' => 'Stok',
    'location' => 'Lokasi',

    // user
    'email' => 'Email',
    'username' => 'Username',
    'password' => 'Kata Sandi',
    'password_confirm' => 'Konfimasi Kata Sandi',
    'password_new' => 'Kata Sandi Baru',
    'level' => 'Level',

    // building
    'address' => 'Alamat',
    'district' => 'Kecamatan',
    'city' => 'Kota',
    'province' => 'Provinsi',
    'postal' => 'Kode Pos',
    'phone' => 'Telephone',
    'website' => 'Website',
    'logo' => 'Logo',
];
