<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master Pengguna',
    'title_create' => 'Buat - Pengguna',
    'title_edit' => 'Ubah - Pengguna',

    'button_create' => 'Buat Pengguna',
    'button_update' => 'Perbarui Pengguna',
    'title_delete' => 'Hapus Pengguna',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Pengguna ini?',
];
