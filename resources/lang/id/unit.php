<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Units Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Master Satuan',
    'title_create' => 'Buat - Satuan',
    'title_edit' => 'Ubah - Satuan',

    'button_create' => 'Buat Satuan',
    'button_update' => 'Perbarui Satuan',
    'title_delete' => 'Hapus Satuan',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Satuan ini?',
];
