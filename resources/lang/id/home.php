<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // general
    'title_detail' => 'Detail - Stok Keluar',
    'title_edit' => 'Ubah - Stok Keluar',
    'title_delete' => 'Hapus Stok Keluar',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Stok Keluar ini?',

    // admin
    'title_stockin' => 'Stok Masuk',
    'subtitle_stockin' => 'Buat Stok Masuk untuk menambah jumlah barang.',
    'title_stockout' => 'Stok Keluar',
    'subtitle_stockout' => ' permohonan Stok Keluar yang harus diproses.',
    
    // user
    'title_transaction' => 'Transaksi',
    'subtitle_transaction' => 'Buat Transaksi untuk memohon Barang.',
    'title_stocknow' => 'Stok Sekarang',
    'subtitle_stocknow' => 'Cek ketersediaan Barang sebelum buat Transaksi.',
];
