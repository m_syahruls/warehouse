<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Stock Opname Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title_create' => 'Buat - Stok Opname',
    'title_summary' => 'Ringkasan - Stok Opname',
    'title_detail' => 'Detail - Stok Opname',
    'title_delete' => 'Hapus Stok Opname',
    'subtitle_delete' => 'Apakah anda yakin ingin menghapus Stok Opname ini?',

    'button_create' => 'Catat Stok Opname',
    'button_clear' => 'Kosongkan Keranjang',
    'button_summary' => 'Ringkasan',
];
