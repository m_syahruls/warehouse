@extends('layouts.app')

@section('content')
<div class="container mt-5 pt-5">
    <div class="row justify-content-center mt-4">
        <div class="col-xl-5 col-md-6">
            <div class="card" style="margin: auto">
                <div class="card-body">
                    <h3 class="d-flex align-items-center justify-content-center my-3 text-primary font-weight-light">@lang('auth.title_login')</h3>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group ">
                            <div>
                                <input id="username" type="username" class="form-control form-login @error('username') is-invalid @enderror" 
                                name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="@lang('auth.username')">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <input id="password" type="password" class="form-control form-login @error('password') is-invalid @enderror" 
                                name="password" required autocomplete="current-password" placeholder="@lang('auth.password')">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" style="font-size: 0.8rem" for="remember" style="display: inline-block">
                                        @lang('auth.remember')
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary btn-login shadow">
                                    @lang('auth.button_login')
                                </button>
                            </div>
                            
                            @if (Route::has('password.request'))
                            <div class="d-flex justify-content-center">
                                <a class="mt-3 small" href="{{ route('password.request') }}">
                                    @lang('auth.forgot')
                                </a>
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection