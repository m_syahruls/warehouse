@extends('layouts.layout')

@section('style')
<link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('css/select2-boostrap.css')}}" rel="stylesheet" />

<style>
  table.dataTable th:nth-child(1) { 
    width: 4%;
    max-width: 10%;
    word-break: break-all;
    white-space: pre-line;
  }
  /* table.dataTable th:nth-child(2) {
    width: 15%;
    min-width: 15%;
    max-width: 15%;
    word-break: break-all;
    white-space: pre-line;
  } */
  table.dataTable td.details-control:before {
   content: '\f152';
   font-family: 'Font Awesome\ 5 Free';
   cursor: pointer;
   font-size: 22px;
   color: #55a4be;
  }
  table.dataTable tr.shown td.details-control:before {
    content: '\f150';
    color: black;
  }
  
  #img-resp{
    width: 30%;
  }

  @media (max-width: 600px) {
    #img-resp{
      width: 100%;
    }
  }

  .file {
    visibility: hidden;
    position: absolute;
  }

  /* body.modal-open .supreme-container{
    -webkit-filter: blur(1px);
    -moz-filter: blur(1px);
    -o-filter: blur(1px);
    -ms-filter: blur(1px);
    filter: blur(1px);
  } */

  /* .modal-backdrop {
    visibility: hidden !important;
  }
  .modal.in {
      background-color: rgba(0,0,0,0.5);
  } */
  /* .select-category {
    width: 100%;
  } */
  .modal {
    overflow-y:auto;
  }
</style>
@endsection

@section('content')
@if ($message = Session::get('error'))
<div class="row" id="proBanner">
  <div class="col-12">
    <span class="d-flex align-items-center purchase-popup alert alert-danger">
      <p>{{ $message }}</p>
      <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
    </span>
  </div>
</div>
@endif
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">@lang('stockin.title_create')</h6>
  </div>
  <div class="card-body">
    <h4>@lang('list.cart')</h4>
    <form action="{{action("StockInController@summary")}}" method="GET">
      @csrf
      <div class ="row mb-3">
        <div class="col-xl-4 col-md-6 col-12">
          <select name="transaction_status" id="transaction_status" class="form-control js-example-basic-single">
            <option value="purchase" selected>Purchase</option>
            @foreach($stocks as $stock)
              <option value="{{ $stock->id }}"><b>{{ $stock->id_transaction }}</b> by {{ $stock->create_by->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-bordered display responsive nowrap" id="table_cart" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>#</th>
              <th>@lang('table.name')</th>
              <th>@lang('table.code')</th>
              <th>@lang('table.qty')</th>
              <th>@lang('table.action')</th>
            </tr>
          </thead>
          <tbody>
              <tr>
                <td colspan="8" style="text-align: center">@lang('table.empty')</td>
              </tr>
          </tbody>
        </table>
      </div>
      <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
        <a href="#" class="btn btn-danger mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2" id="clearBtn">
          <span class="text">@lang('stockin.button_clear')</span>
        </a>
        <button type="submit" class="btn btn-primary">
          <span class="text"><strong>@lang('stockin.button_summary')</strong></span>
        </button>
      </div>
    </form>

    <h4>@lang('list.goodies')</h4>
    <button href="#" class="btn btn-primary btn-icon-split mb-4" style="margin-bottom: 5px;" data-toggle="modal" data-target="#createGoodyModal" id="btnGoodyModal">
      <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
      </span>
      <span class="text">@lang('goody.button_create')</span>
    </button>
    <div class="table-responsive">
      <table class="table table-bordered display responsive nowrap" id="table_goodies" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('table.photo')</th>
            <th>@lang('table.name')</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.qty')</th>
            <th>@lang('table.location')</th>
            <th>@lang('table.action')</th>
          </tr>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="8" style="text-align: center">@lang('table.empty')</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Modal to create new Goody -->
<div id="createGoodyModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg supreme-container">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="createGoodyForm" name="createGoodyForm" method="POST" enctype="multipart/form-data">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">@lang('goody.title_create')</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          {{-- error handling --}}
          <p class="card-description validation-handler">
            <div class="alert alert-danger validation-handler">
              <ul id="validation-errors">
              </ul>
            </div>
          </p>
          <div class="form-group">
            <label for="goodies_category_id">@lang('table.category')</label>
            <div class="input-group select2-bootstrap-append">
              <select class="form-control" id="goodies_category_id" name="goodies_category_id" style="width:96%">
              </select>
              <div class="input-group-append">
                <a class="btn btn-primary btn-sm" href="#" data-toggle="modal" data-target="#createCategoryModal" id="createGoodyBtnCategoryModal"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="goodies_category_code">@lang('table.code')</label>
            <div class="input-group">
              <div class="input-group-append">
                <span class="input-group-text" id="goodies_category_span">@lang('table.category')</span>
                <input type="hidden" id="goodies_category_code" name="goodies_category_code">
              </div>
              <input type="number" class="form-control" id="goodies_code" name="goodies_code" min="1">
            </div>
          </div>
          <div class="form-group">
            <label for="goodies_name">@lang('table.name')</label>
            <input type="text" class="form-control" id="goodies_name" name="goodies_name">
          </div>
          <div class="form-group">
            <label for="goodies_unit">@lang('table.unit')</label>
            <div class="input-group select2-bootstrap-append">
              <select class="form-control" id="goodies_unit" name="goodies_unit" style="width:96%">
              </select>
              <div class="input-group-append">
                <a class="btn btn-primary btn-sm" href="#" data-toggle="modal" data-target="#createUnitModal" id="createGoodyBtnUnitModal"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="location">@lang('table.location')</label>
            <input type="text" class="form-control" id="goodies_location" name="goodies_location">
          </div>
          <div class="form-group">
            <label for="Status">@lang('table.status')</label>
            <div class="form-check">
              <input class="form-check-input goodies_status" type="radio" name="goodies_status" id="status1" value="single" checked>
              <label class="form-check-label" for="status1">
                Single
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input goodies_status" type="radio" name="goodies_status" id="status2" value="multiple">
              <label class="form-check-label" for="status2">
                Multiple
              </label>
            </div>
          </div>
          <div class="form-group">
            <label for="description">@lang('table.description')</label>
            <textarea class="form-control" id="goodies_description" name="goodies_description" cols="30" rows="4"></textarea>
          </div>
          <div class="form-group">
            <label for="img">@lang('table.photo')</label>
            <input type="file" name="photo" id="photo" class="file" accept="image/*">
            <div class="input-group">
              <input type="text" class="form-control" disabled placeholder="Upload File" id="file">
              <div class="input-group-append">
                <button type="button" class="browse btn btn-primary">Browse</button>
              </div>
            </div>
            <img src="{{asset('images/80x80.png')}}" id="preview" class="img-thumbnail my-3">
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.cancel')</button>
          <button type="submit" class="btn btn-primary" id="createGoodyBtn"><strong>@lang('general.create')</strong></button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal to create new Category -->
<div id="createCategoryModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="createCategoryForm" name="createCategoryForm" method="POST" enctype="multipart/form-data">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">@lang('category.title_create')</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <dir class="modal-body">
          <div class="form-group">
            <label for="categories_code">@lang('table.code')</label>
            <input type="text" class="form-control" id="categories_code" name="categories_code" autofocus>
          </div>
          <div class="form-group">
            <label for="categories_name">@lang('table.name')</label>
            <input type="text" class="form-control" id="categories_name" name="categories_name">
          </div>
          <div class="form-group">
              <label for="categories_description">@lang('table.description')</label>
              <textarea class="form-control" id="categories_description" name="categories_description" cols="30" rows="4"></textarea>
          </div>
        </dir>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.cancel')</button>
          <button type="submit" class="btn btn-primary" id="createCategoryBtn"><strong>@lang('general.create')</strong></button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal to create new Unit -->
<div id="createUnitModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="createUnitForm" name="createUnitForm" method="POST" enctype="multipart/form-data">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">@lang('unit.title_create')</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <dir class="modal-body">
          <div class="form-group">
            <label for="units_name">@lang('table.name')</label>
            <input type="text" class="form-control" id="units_name" name="units_name" autofocus>
          </div>
          <div class="form-group">
              <label for="units_description">@lang('table.description')</label>
              <textarea class="form-control" id="units_description" name="units_description" cols="30" rows="4"></textarea>
          </div>
        </dir>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.cancel')</button>
          <button type="submit" class="btn btn-primary" id="createUnitBtn"><strong>@lang('general.create')</strong></button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal to add item to Cart -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="goodyForm" name="goodyForm">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">@lang('modal.title_add_cart')</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <input type="hidden" class="form-control" id="action" name="action" disabled>
          <input type="hidden" class="form-control" id="status" name="status" disabled>
          <input type="hidden" class="form-control" id="id" name="id" disabled>
            <div class="form-group">
              <label for="name">@lang('table.name')</label>
              <div class="input-group">
                <input type="text" class="form-control" id="name" name="name" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="code">@lang('table.code')</label>
              <div class="input-group">
                <input type="text" class="form-control" id="code" name="code" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="code">@lang('table.qty')</label>
              <div class="input-group">
                <input type="number" class="form-control" id="qty" name="qty" min="0" value="1">
              </div>
            </div>
            <input type="hidden" name="description" id="description">
            {{-- <div class="form-group">
              <label for="description">Description</label>
              <div class="input-group">
                <textarea class="form-control" id="description" name="description" cols="30" rows="4"></textarea>
              </div>
            </div> --}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.cancel')</button>
          <button type="submit" class="btn btn-primary" id="addBtn"><strong>@lang('general.add')</strong></button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal to remove item from Cart -->
<div id="removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">@lang('modal.title_delete_cart')</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>@lang('modal.subtitle_delete_cart')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.cancel')</button>
        <button type="" class="btn btn-danger" id="removeBtn"><strong>@lang('modal.yes_remove')</strong></button>
      </div>
    </div>
  </div>
</div>

<!-- Modal for zooming image -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/select2.min.js')}}"></script>
<script src="{{asset('datatables/sorting/natural.js')}}"></script>
<script type="text/javascript">

  // load category list
  var category_reset = function(){
    $.ajax({
      url: "{{ route('api.category.list') }}",
      type: 'GET',
      contentType: 'application/json; charset=utf-8'
    }).then(function (response) {
      $('#goodies_category_id').empty();
      $("#goodies_category_id").select2({
        data: response,
        theme: "bootstrap",
      });
      checkCategory();
    });
  }
  // load unit list
  var unit_reset = function(){
    $.ajax({
      url: "{{ route('api.unit.list') }}",
      type: 'GET',
      contentType: 'application/json; charset=utf-8'
    }).then(function (response) {
      $('#goodies_unit').empty();
      $("#goodies_unit").select2({
        data: response,
        theme: "bootstrap",
      });
    });
  }

  $(document).ready(function() {
    category_reset();
    unit_reset();
  });

// {{-- Focus on create Goodies modal--}}
  var category_id = document.getElementById("goodies_category_id");
  var category_span = document.getElementById("goodies_category_span");
  var category_code = document.getElementById("goodies_category_code");
  var code = document.getElementById("goodies_code");
  $(".validation-handler").hide();


  $(document).on('change', '#goodies_category_id', function() {
    checkCategory();
  });

  $(document).on('click', '#btnGoodyModal', function() {
    $(".validation-handler").hide();
    document.getElementById("preview").src = "{{asset('images/80x80.png')}}";
    category_reset();
    unit_reset();
    category_id = document.getElementById("goodies_category_id");
    // checkCategory();
  });
  
  let checkCategory = function(){
    var check_category_url = '{{ route("api.category.check", ":id") }}';
    check_category_url = check_category_url.replace(':id', category_id.value);
    $.ajax({
      url: check_category_url,
      type: 'GET',
      dataType: 'json',
      success: function(data) {
        category_span.innerHTML = data.category.code;
        category_code.value = data.category.code;
        if (data.goody){
          code.value = parseInt(data.goody.code.split(' ')[1])+1;
        }else{
          code.value = 1;
        }
      },
      error: function(data){
        console.log(data);
      }
    });
  }
  // checkCategory();

  // for image
  $(document).on("click", ".browse", function() {
    var file = $(this).parents().find(".file");
    file.trigger("click");
  });
  $('input[type="file"]').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#file").val(fileName);
    var reader = new FileReader();
    reader.onload = function(e) {
      // get loaded data and render thumbnail.
      document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });

  $(document).ready(function() {
    let urlCart = "{{ route('cart.index') }}";
    let urlList = "{{ route('cart.list') }}";
    
    tableGoodies = $('#table_goodies').DataTable({
      ajax: urlList,
      responsive: true,
      processing:true,
      language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
      columns: [
        // { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: null, name: 'index', orderable: false },
        {
            data: 'photo',
            name: 'photo',
            render: function(data, type, full, meta){
              return "<a href='#' class='pop'><img src={{ asset('images/goodies') }}/" + data + " style='max-height:120px' class='img-thumbnail' alt='" + data + "'/></a>";
            },
            orderable: false
        },
        { data: 'name', name: 'name', },
        { data: 'code_category', name: 'code_category', },
        { data: 'code', name: 'code', },
        { data: 'qty_unit', name: 'qty_unit', },
        { data: 'location', name: 'location', },
        { data: 'action', name: 'action', },
      ],
      orderCellsTop: true,
      columnDefs: [
        { type: 'natural', targets: 3 , visible: false,},
        { type: 'natural', targets: 4 },
        { type: 'natural', targets: 5 },
        { type: 'natural', targets: 6 },
      ],
      order: [[ 4, 'asc' ]],
      initComplete: function () {
        this.api().columns([3]).every( function () {
          var column = this;
          var select = $('<select><option value=""></option></select>')
            .appendTo( $("thead tr:eq(2) th").eq(column.index()).empty() )
            .on( 'change', function () {
              var val = $.fn.dataTable.util.escapeRegex(
                  $(this).val()
              );
              column.search( val ? '^'+val+'$' : '', true, false ).draw();
          } );
          column.data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' );
          } );
        } );
      },
    });
    tableGoodies.on( 'order.dt search.dt', function () {
        tableGoodies.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $(function() {
      $(document).on("click", '.pop', function(event) { 
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#imagemodal').modal('show');
      });
    });

    tableCart = $('#table_cart').DataTable({
      ajax: urlCart,
      responsive: true,
      paging:   false,
      ordering: false,
      info:     false,
      processing:true,
      language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'name', name: 'name', },
        { data: 'code', name: 'code', },
        { data: 'qty', name: 'qty', },
        { data: 'action', name: 'action', },
      ],
    });
  
    // cart data manipulation
    $(function () {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $('body').on('change', '#transaction_status', function () {
        let status = $('#transaction_status').val();
        var url = '{{ route("cart.stock", ":status") }}';
        url = url.replace(':status', status);
        $.get(url, function (data) {
          tableCart.ajax.url(urlCart).load();
        })
      });

      // add item to cart
      $('body').on('click', '#addGoody', function () {
        var goody_id = $(this).data('id');
        $('#addBtn').html("@lang('general.wait')");
        var url = '{{ route("cart.show", ":id") }}';
        url = url.replace(':id', goody_id);
        $.get(url, function (data) {
          $('#addModal').modal('show');
          $('#modalTitle').text("@lang('modal.title_add_cart')");
          $('#action').val("create");
          $('#status').val(data.status);
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#code').val(data.code);
          $('#description').val("");
          $('#addBtn').html("@lang('general.add')");
          $('#addBtn').attr('disabled', false);
          document.getElementById("qty").disabled = false;
          document.getElementById("qty").value = 1;
          document.getElementById("qty").min = 1;
          if(data.status=="single"){
            document.getElementById("qty").max = 1;
            if(data.qty==1){
              $('#addBtn').attr('disabled', true);
              document.getElementById("qty").disabled = true;
            }
          }else{
            document.getElementById("qty").max = "";
          }
          $('#qty').focus();
        })
      });
      // edit item in cart
      $('body').on('click', '#editCart', function () {
        var goody_id = $(this).data('id');
        $('#addBtn').html("@lang('general.wait')");
        var url = '{{ route("cart.edit", ":id") }}';
        url = url.replace(':id', goody_id);
        $.get(url, function (data) {
          $('#addModal').modal('show');
          $('#modalTitle').text("@lang('modal.title_edit_cart')");
          $('#action').val("edit");
          $('#status').val(data.status);
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#code').val(data.code);
          $('#description').val(data.cart_description);
          $('#addBtn').html("@lang('general.update')");
          document.getElementById("qty").value = data.cart_qty;
          document.getElementById("qty").min = 1;
          if(data.status=="single"){
            document.getElementById("qty").max = 1;
          }else{
            document.getElementById("qty").max = "";
          }
          $('#qty').focus();
        })
      });

      // check status single/multiple
      let validd = function(qty, status, action){
        console.log(qty, status, action);
        if (qty == 0){
          alert("@lang('modal.zero_cart')");
          document.getElementById("qty").value = 1;
          $('#addBtn').html("@lang('general.add')");
          if(action == "edit"){
            $('#addBtn').html("@lang('general.update')");
          }
          return false;
        }
        if(status=='single'){
          if (qty > 1) {
            alert("@lang('modal.over_cart_single')");
            document.getElementById("qty").value = 1;
            $('#addBtn').html("@lang('general.add')");
            if(action == "edit"){
              $('#addBtn').html("@lang('general.update')");
            }
            return false;
          }
        }
        return true;
      }
      // store cart to db
      $('#addBtn').click(function (e) {
        e.preventDefault();
        $(this).html("@lang('general.sending')");

        action = $('#action').val();
        status = $('#status').val();
        id = $('#id').val();
        code = $('#code').val();
        qty = $('#qty').val();
        description = $('#description').val();

        if(!validd(qty, status, action)) return false;
        
        let url = "{{ route('cart.store') }}";
        let type = "POST";
        if(action == "edit"){
          url = '{{ route("cart.update", ":id") }}';
          url = url.replace(':id', id);
          type = "PUT";
        }
        
        $.ajax({
          data:{
            "_token": "{{ csrf_token() }}",
            "_method": type,
            id:id,
            code:code,
            qty:qty,
            description:description,
          },
          url: url,
          type: type,
          dataType: 'json',
          success: function (data) {
              $('#goodyForm').trigger("reset");
              $('#addModal').modal('hide');
              $('#addBtn').html("@lang('general.add')");
              console.log(data);
              tableCart.ajax.url(urlCart).load();
          },
          error: function (data) {
              console.log('Error:', data);
              $('#addBtn').html("@lang('general.add')");
          }
        });
      });

      // clear cart
      $('#clearBtn').click(function (e) {
        $('#transaction_status').val("purchase").change();
        clearCart();
      });

      let clearCart = function () {
        $.ajax({
          url: "{{ route('cart.clear') }}",
          type: 'GET',
          dataType: 'json',
          success: function (data) {
            console.log(data);
            tableCart.ajax.url(urlCart).load();
          },
          error: function (data) {
            console.log('Error:', data);
          }
        });
      }
      clearCart();

      // remove item from cart
      $('#removeModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var id = button.data('id'); // Extract info from data-* attributes
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        var form = document.getElementById("deleteForm");
        url = '{{ route("cart.destroy", ":id") }}';
        url = url.replace(':id', id);
        $('#removeBtn').click(function (e) {
          $.ajax({
            data:{
              "_token": "{{ csrf_token() }}",
              "_method": "DELETE",
              id:id,
            },
            url: url,
            type: "DELETE",
            dataType: 'json',
            success: function (data) {
                $('#removeModal').modal('hide');
                console.log(data);
                tableCart.ajax.url(urlCart).load();
            },
            error: function (data) {
                console.log('Error:', data);
            }
          });
        });
      })
    });

    // for create new goodies
    $(function () {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      //store goodies to db
      $('#createGoodyBtn').click(function (e) {
        e.preventDefault();
        $(this).html("@lang('general.sending')");

        var formData = new FormData();
        formData.append('_method', 'POST' );
        formData.append("category_id", $('#goodies_category_id').val() );
        formData.append("category_code", $('#goodies_category_code').val() );
        formData.append("code", $('#goodies_code').val() );
        formData.append("name", $('#goodies_name').val() );
        formData.append("unit", $('#goodies_unit').val() );
        formData.append("location", $('#goodies_location').val() );
        formData.append("status", $('.goodies_status:checked').val() );
        formData.append("description", $('#goodies_description').val() );
        formData.append("photo", $('input[type=file]')[0].files[0] );

        let url = "{{ route('goody.store_stockin') }}";
        let type = "POST";
        
        $.ajax({
          data: formData,
          url: url,
          type: type,
          dataType: 'json',
          processData: false,
          contentType: false,
          success: function (data) {
            $(".validation-handler").hide();
            $('#createGoodyForm').trigger("reset");
            $('#createGoodyModal').modal('hide');
            $('#createGoodyBtn').html("@lang('general.create')");
            console.log(data);
            tableGoodies.ajax.url(urlList).load();
          },
          error: function (data) {
            $(".validation-handler").show();
            $('#validation-errors').html('');
            $.each(data.responseJSON.errors, function(key,value) {
                $('#validation-errors').append('<li>'+value+'</li>');
            });
            $('#createGoodyBtn').html("@lang('general.create')");
          }
        });
      });
    });

    // for create new category
    $(function () {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      //store category to db
      $('#createCategoryBtn').click(function (e) {
        e.preventDefault();
        $(this).html("@lang('general.sending')");

        var formData = new FormData();
        formData.append('_method', 'POST' );
        formData.append("code", $('#categories_code').val() );
        formData.append("name", $('#categories_name').val() );
        formData.append("description", $('#categories_description').val() );

        let url = "{{ route('category.store_stockin') }}";
        let type = "POST";
        
        $.ajax({
          data: formData,
          url: url,
          type: type,
          dataType: 'json',
          processData: false,
          contentType: false,
          success: function (data) {
            $('#createCategoryForm').trigger("reset");
            $('#createCategoryModal').modal('hide');
            $('#createCategoryBtn').html("@lang('general.create')");
            console.log(data);
            category_reset()
          },
          error: function (data) {
            console.log('Error:', data);
            $('#createCategoryBtn').html("@lang('general.create')");
          }
        });
      });
    });

    // for create new unit
    $(function () {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      //store unit to db
      $('#createUnitBtn').click(function (e) {
        e.preventDefault();
        $(this).html("@lang('general.sending')");

        var formData = new FormData();
        formData.append('_method', 'POST' );
        formData.append("name", $('#units_name').val() );
        formData.append("description", $('#units_description').val() );

        let url = "{{ route('unit.store_stockin') }}";
        let type = "POST";
        
        $.ajax({
          data: formData,
          url: url,
          type: type,
          dataType: 'json',
          processData: false,
          contentType: false,
          success: function (data) {
            $('#createUnitForm').trigger("reset");
            $('#createUnitModal').modal('hide');
            $('#createUnitBtn').html("@lang('general.create')");
            console.log(data);
            unit_reset();
          },
          error: function (data) {
            console.log('Error:', data);
            $('#createUnitBtn').html("@lang('general.create')");
          }
        });
      });
    });
  })
</script>
@endsection