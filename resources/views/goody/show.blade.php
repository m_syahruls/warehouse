@extends('layouts.layout')

@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-lg-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">@lang('goody.title_show')</h5>
      </div>
      <div class="card-body">
        <div class="form-group">
          <label for="name">@lang('table.category')</label>
          <div class="input-group">
            <input type="text" class="form-control" id="category_id" name="category_id" readonly disabled value="{{ $goody->category->code }} | {{ $goody->category->name }}">
          </div>
        </div>
        <div class="form-group">
          <label for="code">@lang('table.code')</label>
          <input type="text" class="form-control" id="code" name="code" readonly disabled value="{{$goody->code}}">
        </div>
        <div class="form-group">
          <label for="name">@lang('table.name')</label>
          <input type="text" class="form-control" id="name" name="name" readonly disabled value="{{$goody->name}}">
        </div>
        <div class="form-group">
          <label for="name">@lang('table.unit')</label>
          <input type="text" class="form-control" id="unit" name="unit" readonly disabled value="{{$goody->unit->name}}">
        </div>
        <div class="form-group">
          <label for="location">@lang('table.location')</label>
          <input type="text" class="form-control" id="location" name="location" readonly disabled value="{{$goody->location}}">
        </div>
        <div class="form-group">
          <label for="status">@lang('table.status')</label>
          <input type="text" class="form-control" id="status" name="status" readonly disabled value="{{ucfirst($goody->status)}}">
        </div>
        <div class="form-group">
          <label for="description">@lang('table.description')</label>
          <textarea class="form-control" id="description" name="description" cols="30" rows="4" readonly disabled>{{$goody->description}}</textarea>
        </div>
        <div class="form-group">
          <label for="img">@lang('table.photo')</label>
          <div class="input-group">
            <img src="{{ asset('images/goodies') }}/{{ $goody->photo }}" id="preview" class="img-thumbnail"/>
          </div>
        </div>
        <div class="row mb-1">
          <div class="col">
            <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
              <a type="button" href="{{action('GoodyController@index')}}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.back')</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>
@endsection
