@extends('layouts.layout')

@section('style')
<style>
  /* table.dataTable th:nth-child(1) {
    width: 4%;
    max-width: 10%;
    word-break: break-all;
    white-space: pre-line;
  } */
  table.dataTable th:nth-child(1) { 
    width: 4%;
    max-width: 10%;
    word-break: break-all;
    white-space: pre-line;
  }
  /* table.dataTable th:nth-child(2) {
    width: 15%;
    min-width: 15%;
    max-width: 15%;
    word-break: break-all;
    white-space: pre-line;
  } */
  table.dataTable td.details-control:before {
   content: '\f152';
   font-family: 'Font Awesome\ 5 Free';
   cursor: pointer;
   font-size: 22px;
   color: #55a4be;
  }
  table.dataTable tr.shown td.details-control:before {
    content: '\f150';
    color: black;
  }
  
  #img-resp{
    width: 30%;
  }

  @media (max-width: 600px) {
    #img-resp{
      width: 100%;
    }
  }
</style>
@endsection

@section('content')
@if ($message = Session::get('success'))
<div class="row" id="proBanner">
  <div class="col-12">
    <span class="d-flex align-items-center purchase-popup alert alert-success">
      <p>{{ $message }}</p>
      <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
    </span>
  </div>
</div>
@endif
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">@lang('goody.title')</h6>
  </div>
  <div class="card-body">
    <a href="{{action('GoodyController@create')}}" class="btn btn-primary btn-icon-split mb-4" style="margin-bottom: 5px;">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text"><strong>@lang('goody.button_create')</strong></span>
    </a>
    <div class="table-responsive">
      <table class="table table-bordered display responsive nowrap" id="table" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('table.photo')</th>
            <th>@lang('table.name')</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.qty')</th>
            <th>@lang('table.unit')</th>
            <th>@lang('table.location')</th>
            <th>@lang('table.action')</th>
          </tr>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="9" style="text-align: center">@lang('table.empty')</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">@lang('goody.title_delete')</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>@lang('goody.subtitle_delete')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.no')</button>
        <form action="#" method="POST" id="deleteForm">
          @csrf
          {{ method_field('DELETE') }}
          <button type="submit" class="btn btn-danger" id="delete-btn"><strong>@lang('modal.yes_delete')</strong></button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal for zooming image -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('datatables/sorting/natural.js')}}"></script>
<script>
  $(document).ready(function() {
    table = $('#table').DataTable({
      ajax: "{{ route('api.goody.list') }}",
      responsive: true,
      processing:true,
      language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
      columns: [
        // {
        //   className: 'details-control',
        //   orderable: false,
        //   searchable: false,
        //   defaultContent: '',
        //   visible: ''
        // },
        // { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: null, name: 'index', orderable: false },
        {
            data: 'photo',
            name: 'photo',
            render: function(data, type, full, meta){
              return "<a href='#' class='pop'><img src={{ asset('images/goodies') }}/" + data + " style='max-height:120px' class='img-thumbnail' alt='" + data + "'/></a>";
            },
            orderable: false
        },
        { data: 'name', name: 'name', },
        { data: 'code_category', name: 'code_category', },
        { data: 'code', name: 'code', },
        { data: 'qty', name: 'qty', },
        { data: 'unit.name', name: 'unit', },
        { data: 'location', name: 'location', },
        {
          data: 'action', 
          name: 'action', 
          orderable: false, 
          searchable: false,
          defaultContent: ''
        },
      ],
      orderCellsTop: true,
      columnDefs: [
          { type: 'natural', targets: 3 , visible: false,},
          { type: 'natural', targets: 4 },
          { type: 'natural', targets: 5 },
          { type: 'natural', targets: 6 },
          { type: 'natural', targets: 7 },
      ],
      order: [[ 4, 'asc' ]],
      initComplete: function () {
        this.api().columns([3]).every( function () {
          var column = this;
          var select = $('<select><option value=""></option></select>')
            .appendTo( $("thead tr:eq(1) th").eq(column.index()).empty() )
            .on( 'change', function () {
                var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                );
                column.search( val ? '^'+val+'$' : '', true, false ).draw();
          } );
          column.data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' );
          } );
        } );
      },
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
  })

  $('#deleteModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('id'); // Extract info from data-* attributes
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    var form = document.getElementById("deleteForm");
    var url = '{{ action("GoodyController@destroy", ":id") }}';
    url = url.replace(':id', id);
    form.action = url;
  })

  $(function() {
    $(document).on("click", '.pop', function(event) { 
      $('.imagepreview').attr('src', $(this).find('img').attr('src'));
      $('#imagemodal').modal('show');
    });
  });
</script>
@endsection