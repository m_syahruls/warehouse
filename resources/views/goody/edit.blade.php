@extends('layouts.layout')

@section('style')
<link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('css/select2-boostrap.css')}}" rel="stylesheet" />

<style>
  .file {
    visibility: hidden;
    position: absolute;
  }
</style>
@endsection

@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-lg-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">@lang('goody.title_edit')</h5>
      </div>
      <div class="card-body">
        @if (count($errors) > 0)
          <p class="card-description"> 
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          </p>
        @endif
        <form action="{{action('GoodyController@update', $goody->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        {{ method_field('PUT') }}
          <div class="form-group">
            <label for="category_id">@lang('table.category')</label>
            <div class="input-group select2-bootstrap-append">
              <select class="form-control" id="category_id" name="category_id" style="width:93%"></select>
              <div class="input-group-append">
                <a class="btn btn-primary btn-sm" href="#" data-toggle="modal" data-target="#createCategoryModal" id="createGoodyBtnCategoryModal"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="code">@lang('table.code')</label>
            <div class="input-group">
              <div class="input-group-append">
                <span class="input-group-text" id="category_span">@lang('table.code')</span>
                <input type="hidden" id="category_code" name="category_code">
              </div>
              <input type="number" class="form-control" id="code" name="code" min="1">
            </div>
          </div>
          <div class="form-group">
            <label for="name">@lang('table.name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$goody->name}}">
          </div>
          <div class="form-group">
            <label for="unit">@lang('table.unit')</label>
            <div class="input-group select2-bootstrap-append">
              <select class="form-control" id="unit" name="unit" style="width:93%"></select>
              <div class="input-group-append">
                <a class="btn btn-primary btn-sm" href="#" data-toggle="modal" data-target="#createUnitModal" id="createGoodyBtnUnitModal"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="location">@lang('table.location')</label>
            <input type="text" class="form-control" id="location" name="location" value="{{$goody->location}}">
          </div>
          <div class="form-group">
            <label for="Status">@lang('table.status')</label>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="status" id="status1" value="single" {{ $goody->status == 'single' ? 'checked' : ''}}>
              <label class="form-check-label" for="status1">
                Single
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="status" id="status2" value="multiple" {{ $goody->status == 'multiple' ? 'checked' : ''}}>
              <label class="form-check-label" for="status2">
                Multiple
              </label>
            </div>
          </div>
          <div class="form-group">
            <label for="description">@lang('table.description')</label>
            <textarea class="form-control" id="description" name="description" cols="30" rows="4">{{$goody->description}}</textarea>
          </div>
          <div class="form-group">
            <label for="img">@lang('table.photo')</label>
            <input type="hidden" name="hidden_photo" value="{{ $goody->photo }}" />
            <input type="file" name="photo" class="file" accept="image/*">
            <div class="input-group">
              <input type="text" class="form-control" disabled placeholder="Upload File" id="file">
              <div class="input-group-append">
                <button type="button" class="browse btn btn-primary">Browse</button>
              </div>
            </div>
            <img src="{{ asset('/images/goodies') }}/{{ $goody->photo }}" id="preview" class="img-thumbnail my-3"/>
          </div>
          <div class="row mb-1">
            <div class="col">
              <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                <a type="button" href="{{action('GoodyController@index')}}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.cancel')</a>
                <button type="submit" class="btn btn-primary"><strong>@lang('goody.button_update')</strong></button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>

<!-- Modal to create new Category -->
<div id="createCategoryModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="createCategoryForm" name="createCategoryForm" method="POST" enctype="multipart/form-data">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">@lang('category.title_create')</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <dir class="modal-body">
          <div class="form-group">
            <label for="categories_code">@lang('table.code')</label>
            <input type="text" class="form-control" id="categories_code" name="categories_code">
          </div>
          <div class="form-group">
            <label for="categories_name">@lang('table.name')</label>
            <input type="text" class="form-control" id="categories_name" name="categories_name">
          </div>
          <div class="form-group">
              <label for="categories_description">@lang('table.description')</label>
              <textarea class="form-control" id="categories_description" name="categories_description" cols="30" rows="4"></textarea>
          </div>
        </dir>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.no')</button>
          <button type="submit" class="btn btn-primary" id="createCategoryBtn"><strong>@lang('category.button_create')</strong></button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal to create new Unit -->
<div id="createUnitModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="createUnitForm" name="createUnitForm" method="POST" enctype="multipart/form-data">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">@lang('unit.title_create')</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <dir class="modal-body">
          <div class="form-group">
            <label for="units_name">@lang('table.name')</label>
            <input type="text" class="form-control" id="units_name" name="units_name">
          </div>
          <div class="form-group">
              <label for="units_description">@lang('table.description')</label>
              <textarea class="form-control" id="units_description" name="units_description" cols="30" rows="4"></textarea>
          </div>
        </dir>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.no')</button>
          <button type="submit" class="btn btn-primary" id="createUnitBtn"><strong>@lang('unit.button_create')</strong></button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
  var category_id = document.getElementById("category_id");
  var category_span = document.getElementById("category_span");
  var category_code = document.getElementById("category_code");
  var code = document.getElementById("code");

  // load category list
  var category_reset = function(){
    var category_url = '{{ route("api.category.list.selected", $goody->category_id) }}';
    $.ajax({
      url: category_url,
      type: 'GET',
      contentType: 'application/json; charset=utf-8'
    }).then(function (response) {
      $('#category_id').empty();
      $("#category_id").select2({
        data: response,
        theme: "bootstrap",
      });
      checkCategory();
    });
  }
  // load unit list
  var unit_reset = function(){
    $.ajax({
      url: "{{ route('api.unit.list') }}",
      type: 'GET',
      contentType: 'application/json; charset=utf-8'
    }).then(function (response) {
      $('#unit').empty();
      $("#unit").select2({
        data: response,
        theme: "bootstrap",
      });
    });
  }

  // select2
  $(document).ready(function() {
    category_reset();
    unit_reset();
  });

  $(document).on('change', '#category_id', function() {
    checkCategory();
  });

  let checkCategory = function(){
    var check_category_url = '{{ route("api.category.check", ":id") }}';
    check_category_url = check_category_url.replace(':id', category_id.value);
    $.ajax({
      url: check_category_url,
      type: 'GET',
      dataType: 'json',
      success: function(data) {
        category_span.innerHTML = data.category.code;
        category_code.value = data.category.code;
        if (data.goody){
          let getCode = '{{$goody->code}}';
          if (data.goody.code.split(' ')[0] == getCode.split(' ')[0]){
            code.value = getCode.split(' ')[1];
          }else{
            code.value = parseInt(data.goody.code.split(' ')[1])+1;
          }
        }else{
          code.value = 1;
        }
      },
      error: function(data){
        console.log(data);
      }
    });
  }

  //for image
  $(document).on("click", ".browse", function() {
    var file = $(this).parents().find(".file");
    file.trigger("click");
  });
  $('input[type="file"]').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#file").val(fileName);

    var reader = new FileReader();
    reader.onload = function(e) {
      // get loaded data and render thumbnail.
      document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });
  
  $(document).ready(function() {
        // for create new category
      $(function () {
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        //store category to db
        $('#createCategoryBtn').click(function (e) {
          e.preventDefault();
          $(this).html("@lang('general.sending')");

          var formData = new FormData();
          formData.append('_method', 'POST' );
          formData.append("code", $('#categories_code').val() );
          formData.append("name", $('#categories_name').val() );
          formData.append("description", $('#categories_description').val() );

          let url = "{{ route('category.store_stockin') }}";
          let type = "POST";
          
          $.ajax({
            data: formData,
            url: url,
            type: type,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
              $('#createForm').trigger("reset");
              $('#createCategoryModal').modal('hide');
              $('#createCategoryBtn').html("@lang('general.create')");
              console.log(data);
              var category_url = '{{ route("api.category.list.selected", $goody->category_id) }}';
              category_reset();
            },
            error: function (data) {
              console.log('Error:', data);
              $('#createCategoryBtn').html("@lang('general.create')");
            }
          });
        });
      });

      // for create new unit
      $(function () {
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        //store unit to db
        $('#createUnitBtn').click(function (e) {
          e.preventDefault();
          $(this).html("@lang('general.sending')");

          var formData = new FormData();
          formData.append('_method', 'POST' );
          formData.append("name", $('#units_name').val() );
          formData.append("description", $('#units_description').val() );

          let url = "{{ route('unit.store_stockin') }}";
          let type = "POST";
          
          $.ajax({
            data: formData,
            url: url,
            type: type,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
              $('#createForm').trigger("reset");
              $('#createUnitModal').modal('hide');
              $('#createUnitBtn').html("@lang('general.create')");
              console.log(data);
              var unit_url = '{{ route("api.unit.list.selected", $goody->unit_id) }}';
              unit_reset();
            },
            error: function (data) {
              console.log('Error:', data);
              $('#createUnitBtn').html("@lang('general.create')");
            }
          });
        });
      });
    })
</script>
@endsection