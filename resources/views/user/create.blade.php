@extends('layouts.layout')

@section('style')
<style>
  .file {
    visibility: hidden;
    position: absolute;
  }
</style>
@endsection

@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-lg-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">@lang('user.title_create')</h5>
      </div>
      <div class="card-body">
        @if (count($errors) > 0)
          <p class="card-description"> 
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          </p>
        @endif
        <form action="{{action('UserController@store')}}" method="POST" enctype="multipart/form-data">
        @csrf
          <div class="form-group">
            <label for="img">@lang('table.photo')</label>
            <input type="file" name="photo" class="file" accept="image/*">
            <div class="input-group">
              <input type="text" class="form-control" disabled placeholder="Upload File" id="file">
              <div class="input-group-append">
                <button type="button" class="browse btn btn-primary">Browse</button>
              </div>
            </div>
            <img src="{{asset('images/80x80.png')}}" id="preview" class="img-thumbnail my-3" width="200px">
          </div>
          <div class="form-group">
            <label for="name">@lang('table.name')</label>
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
          </div>
          <div class="form-group">
            <label for="username">@lang('table.username')</label>
            <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username">
          </div>
          <div class="form-group">
            <label for="email">@lang('table.email')</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
          </div>
          <div class="form-group">
            <label for="password">@lang('table.password')</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
          </div>
          <div class="form-group">
            <label for="password-confirm">@lang('table.password_confirm')</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
          </div>
          <div class="form-group">
            <label for="level">@lang('table.level')</label>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="level" id="level1" value="admin" checked>
              <label class="form-check-label" for="level1">
                Admin
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="level" id="level2" value="user">
              <label class="form-check-label" for="level2">
                User
              </label>
            </div>
          </div>
          <div class="row mb-1">
            <div class="col">
              <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                <a type="button" href="{{action('UserController@index')}}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.cancel')</a>
                <button type="submit" class="btn btn-primary"><strong>@lang('user.button_create')</strong></button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>
@endsection

@section('script')
<script>
  //for image
  $(document).on("click", ".browse", function() {
    var file = $(this).parents().find(".file");
    file.trigger("click");
  });
  $('input[type="file"]').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#file").val(fileName);

    var reader = new FileReader();
    reader.onload = function(e) {
      // get loaded data and render thumbnail.
      document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });
</script>
@endsection