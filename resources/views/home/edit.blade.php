@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col-lg-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h5 class="m-0 font-weight-bold text-primary">@lang('home.title_edit')</h5>
            </div>
            <div class="card-body">
                @if (count($errors) > 0)
                    <p class="card-description"> 
                        <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        </div>
                    </p>
                @endif
                <form action="{{action('HomeController@update', $stock->id)}}" method="POST">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="id_transaction">@lang('table.id')</label>
                        <input type="text" class="form-control" name="id_transaction" id="id_transaction" readonly disabled value="{{$stock->id_transaction}}">
                    </div>
                    <div class="form-group">
                        <label for="date">@lang('table.date')</label>
                        <input type="text" class="form-control" name="date" id="date" readonly disabled value="{{$stock->date}}">
                    </div>
                    <div class="form-group">
                        <label for="created_by">@lang('table.created_by')</label>
                        <input type="text" class="form-control" name="user_created" id="user_created" readonly disabled value="{{$stock->create_by->name}}">
                    </div>
                    <div class="form-group">
                        <label for="updated_by">@lang('table.updated_by')</label>
                        <input type="hidden" name="updated_by" id="updated_by" value="{{isset(Auth::user()->id)? Auth::user()->id :""}}">
                        <input type="text" class="form-control" name="user_updated" id="user_updated" readonly disabled value="{{isset(Auth::user()->name)? Auth::user()->name :""}}">
                    </div>
                    <div class="form-group">
                        <label for="name">@lang('list.goodies')</label>
                        <div class="table-responsive">
                            <table class="table table-bordered display responsive nowrap" id="table" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('table.name')</th>
                                        <th>@lang('table.code')</th>
                                        <th>@lang('table.stock')</th>
                                        <th>@lang('table.qty')</th>
                                        <th>@lang('table.description')</th>
                                        <th>@lang('table.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">@lang('table.description')</label>
                        <div class="input-group">
                            <textarea readonly class="form-control" id="description_form" name="description_form" cols="30" rows="4">{{$stock->description}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status">@lang('table.status')</label>
                        <input type="text" class="form-control" id="status" readonly disabled value="{{ucfirst($stock->status)}}">
                    </div>
                    <div class="form-group">
                        <label for="confirmation">@lang('table.confirmation')</label>
                        <select name="confirmation" id="confirmation" class="form-control">
                            <option value="unconfirmed" {{ $stock->confirmation == "unconfirmed" ? 'selected' : '' }}>Unconfirmed</option>
                            <option value="reject" {{ $stock->confirmation == "reject" ? 'selected' : '' }}>Reject</option>
                            <option value="progress" {{ $stock->confirmation == "progress" ? 'selected' : '' }}>Progress</option>
                            <option value="done" {{ $stock->confirmation == "done" ? 'selected' : '' }}>Done</option>
                        </select>
                    </div>
                    <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                        <a type="button" href="{{action('HomeController@index')}}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.back')</a>
                        <button type="submit" class="btn btn-primary"><strong>@lang('general.update')</strong></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>

<!-- Modal -->
<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <form action="" id="itemForm" name="itemForm">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">@lang('modal.title_item')</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <input type="hidden" class="form-control" id="id" name="id" disabled>
                <div class="form-group">
                    <label for="name">@lang('table.name')</label>
                    <div class="input-group">
                      <input type="text" class="form-control" id="name" name="name" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="code">@lang('table.code')</label>
                    <div class="input-group">
                      <input type="text" class="form-control" id="code" name="code" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="qty">@lang('table.qty_request')</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="qty" name="qty" min="0" value="1" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="qty_confirm">@lang('table.qty_confirm')</label>
                    <div class="input-group">
                      <input type="number" class="form-control" id="qty_confirm" name="qty_confirm" min="0" value="1">
                    </div>
                </div>
                <div class="form-group">
                  <label for="description">@lang('table.description')</label>
                  <div class="input-group">
                    <textarea class="form-control" id="description" name="description" cols="30" rows="4"></textarea>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.cancel')</button>
              <button type="submit" class="btn btn-primary" id="editBtn"><strong>@lang('general.add')</strong></button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var urlItem = "{{ route('stockout.item.show', $stock->id) }}";
        table = $('#table').DataTable({
            ajax: urlItem,
            responsive: true,
            paging:   false,
            ordering: false,
            info:     false,
            searching: false,
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'name', name: 'name', },
                { data: 'code', name: 'code', },
                // { data: 'qty_confirm', name: 'qty', },
                { data: "stock", name: 'stock', 
                    render: function ( data, type, row ) {
                        return row.stock + ' ' + row.unit;
                    }
                },
                { data: "qty_confirm", name: 'qty', 
                    render: function ( data, type, row ) {
                        return row.qty_confirm + ' ' + row.unit;
                    }
                },
                { data: 'description', name: 'description', },
                { data: 'action', name: 'action', },
            ],
        });

        $(function () {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $('body').on('click', '#editItem', function () {
                var item_id = $(this).data('id');
                $('#editBtn').html("@lang('general.wait')");
                var url = '{{ route("stockout.item.edit", ":id") }}';
                url = url.replace(':id', item_id);
                $.get(url, function (data) {
                    $('#editModal').modal('show');
                    $('#id').val(data.id);
                    $('#name').val(data.name);
                    $('#code').val(data.code);
                    $('#description').val(data.description);
                    $('#editBtn').html("@lang('general.update')");
                    document.getElementById("qty").value = data.qty;
                    document.getElementById("qty_confirm").value = data.qty_confirm;
                    document.getElementById("qty_confirm").min = 0;
                    document.getElementById("qty_confirm").max = data.qty;
                })
            });

            // check status single/multiple
            let validd = function(qty, request){
                if (qty > parseInt(request)) {
                    alert("@lang('modal.over_cart_multiple')");
                    document.getElementById("qty_confirm").value = request;
                    $('#editBtn').html("@lang('general.update')");
                    return false;
                }
                console.log(qty+'-'+request);
                return true;
            }

            $('#editBtn').click(function (e) {
                e.preventDefault();
                $(this).html("@lang('general.sending')");
                id = $('#id').val();
                request = $('#qty').val();
                qty = $('#qty_confirm').val();
                description = $('#description').val();

                if(!validd(qty, request)) return false;

                var url = '{{ route("stockout.item.update", ":id") }}';
                url = url.replace(':id', id);
                let type = "PUT";
                
                $.ajax({
                    data:{
                        "_token": "{{ csrf_token() }}",
                        "_method": type,
                        id:id,
                        qty:qty,
                        description:description,
                    },
                    url: url,
                    type: type,
                    dataType: 'json',
                    success: function (data) {
                        $('#itemForm').trigger("reset");
                        $('#editModal').modal('hide');
                        $('#editBtn').html("@lang('general.update')");
                        console.log(data);
                        table.ajax.url(urlItem).load();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#editBtn').html("@lang('general.update')");
                    }
                });
            });
        });
    })
</script>
@endsection