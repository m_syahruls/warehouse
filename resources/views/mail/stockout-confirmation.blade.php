@component('mail::message')
<style>
  .table{
    width: 100%;
    border-collapse: collapse;
  }

  .to-right{
    text-align: right;
  }

  .items-table td, th{
        border: 1px solid #EDEFF2;
        padding: 5px;
    }
</style>
# {{ $stock->id_transaction }} transaction has been updated

Your transaction with ID <strong>{{ $stock->id_transaction }}</strong> has been updated to <strong>{{ ucfirst($stock->confirmation) }}</strong><br>
<small>
  by {{ $stock->update_by->name }}<br>
  at {{ \Carbon\Carbon::parse($stock->updated_at)->format('d M Y h:i:s A') }}<br>
</small>

<strong>Transaction details: </strong><br>
<table class="table">
  <tr>
    <td>Transaction ID</td>
    <td class="to-right">{{ $stock->id_transaction }}</td>
  </tr>
  <tr>
    <td>Created by</td>
    <td class="to-right">{{ $stock->create_by->name }}</td>
  </tr>
  <tr>
    <td>Created at</td>
    <td class="to-right">{{ \Carbon\Carbon::parse($stock->date)->format('d M Y') }}</td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="to-right">{{ ucfirst($stock->status) }}</td>
  </tr>
  <tr>
    <td>Confirmation</td>
    <td class="to-right">{{ ucfirst($stock->confirmation) }}</td>
  </tr>
  <tr>
    <td colspan="2">Description</td>
  </tr>
  <tr>
    <td colspan="2">{{ $stock->description }}</td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%" cellspacing="0" border="1" class="items-table" style="border-collapse: collapse; border: 1px solid black;">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Code</th>
                <th>Qty</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($stock->items as $item)
                <tr>
                    <td width="4%">{{$loop->iteration}}</td>
                    <td>{{$item->goody->name}}</td>
                    <td><nobr>{{$item->goody->code}}</nobr></td>
                    <td><nobr>{{$item->qty_confirm}} {{$item->goody->unit->name}}</nobr></td>
                    <td class="to-right">{{$item->description}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" style="text-align: center">Empty Data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    </td>
  </tr>
</table>
<br>

Click below to check your home
@component('mail::button', ['url' => $link])
Go to Home
@endcomponent

Feel free to contact us for any further information.<br><br>
Regards,<br>
Admin at Warehouse
@endcomponent
