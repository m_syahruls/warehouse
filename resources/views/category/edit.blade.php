@extends('layouts.layout')

@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-lg-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">@lang('category.title_edit')</h5>
      </div>
      <div class="card-body">
        @if (count($errors) > 0)
          <p class="card-description"> 
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          </p>
        @endif
        <form action="{{action('CategoryController@update', $category->id)}}" method="POST">
        @csrf
        {{ method_field('PUT') }}
          <div class="form-group">
            <label for="code">@lang('table.code')</label>
            <input type="text" class="form-control" id="code" name="code" value="{{$category->code}}">
          </div>
          <div class="form-group">
            <label for="name">@lang('table.name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$category->name}}">
          </div>
          <div class="form-group">
            <label for="description">@lang('table.description')</label>
            <textarea class="form-control" id="description" name="description" cols="30" rows="4">{{$category->description}}</textarea>
          </div>
          <div class="row mb-1">
            <div class="col">
              <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                <a type="button" href="{{action('CategoryController@index')}}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.cancel')</a>
                <button type="submit" class="btn btn-primary"><strong>@lang('category.button_update')</strong></button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>
@endsection