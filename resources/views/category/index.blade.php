@extends('layouts.layout')

@section('content')
@if ($message = Session::get('success'))
<div class="row" id="proBanner">
  <div class="col-12">
    <span class="d-flex align-items-center purchase-popup alert alert-success">
      <p>{{ $message }}</p>
      <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
    </span>
  </div>
</div>
@endif
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">@lang('category.title')</h6>
  </div>
  <div class="card-body">
    <a href="{{action('CategoryController@create')}}" class="btn btn-primary btn-icon-split mb-4" style="margin-bottom: 5px;">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text"><strong>@lang('category.button_create')</strong></span>
    </a>
    <div class="table-responsive">
      <table class="table table-bordered" id="table" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.name')</th>
            <th>@lang('table.description')</th>
            <th>@lang('table.action')</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($categories as $category)
            <tr>
              <td width="4%">{{$loop->iteration}}</td>
              <td>{{$category->code}}</td>
              <td>{{$category->name}}</td>
              <td>{{$category->description}}</td>
              <td width="9%">
                <a href="{{action('CategoryController@edit', $category->id)}}" class="btn btn-warning btn-circle btn-sm mb-1">
                  <i class="fas fa-edit"></i>
                </a>
                <a href="#" class="btn btn-danger btn-circle btn-sm mb-1" data-toggle="modal" data-target="#deleteModal" data-id="{{$category->id}}">
                  <i class="fas fa-trash"></i>
                </a>
              </td>
            </tr>
          @empty
            {{-- <tr>
              <td colspan="7" style="text-align: center">@lang('table.empty')</td>
            </tr> --}}
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">@lang('category.title_delete')</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>@lang('category.subtitle_delete')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.no')</button>
        <form action="#" method="POST" id="deleteForm">
          @csrf
          {{ method_field('DELETE') }}
          <button type="submit" class="btn btn-danger" id="delete-btn"><strong>@lang('modal.yes_delete')</strong></button>
        </form>  
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
<script>
  $(document).ready(function() {
    $('#table').DataTable();
  })

  $('#deleteModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('id'); // Extract info from data-* attributes
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    var form = document.getElementById("deleteForm");
    var url = '{{ action("CategoryController@destroy", ":id") }}';
    url = url.replace(':id', id);
    form.action = url;
  })
</script>
@endsection