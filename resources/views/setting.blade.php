@extends('layouts.layout')

@section('style')
<style>
  .file {
    visibility: hidden;
    position: absolute;
  }
</style>
@endsection

@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-lg-8">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">@lang('setting.title')</h6>
      </div>
      <div class="card-body m-2">
        @if ($message = Session::get('success'))
        <div class="row" id="proBanner">
          <div class="col-12">
            <span class="d-flex align-items-center purchase-popup alert alert-success">
              <p>{{ $message }}</p>
              <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
            </span>
          </div>
        </div>
        @elseif (count($errors) > 0)
        <div class="row" id="proBanner">
          <div class="col-12">
            <span class="d-flex align-items-center purchase-popup alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
              <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
            </span>
          </div>
        </div>
        @endif

        <div class="nav nav-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-building-tab" data-toggle="tab" href="#nav-building" role="tab" aria-controls="nav-rate" aria-selected="false">@lang('setting.building')</a>
        </div>
        <div class="tab-content" id="nav-tabContent">

          <div class="tab-pane fade mt-4 show active" id="nav-building" role="tabpanel" aria-labelledby="nav-building-tab">
            <div class="row">
              <div class="col">
                  <form action="{{action('SettingController@storeBuilding')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group">
                      <label for="node_id">@lang('table.name')</label>
                      <input type="text" class="form-control" id="building_name" name="building_name" value="{{isset($building->name)?$building->name:''}}" placeholder="-">
                    </div>
                    <div class="form-group">
                      <label for="node_id">@lang('table.address')</label>
                      <textarea name="address" class="form-control" id="address" cols="30" rows="2" placeholder="-">{{isset($building->address)?$building->address:''}}</textarea>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="node_id">@lang('table.district')</label>
                        <input type="text" class="form-control" id="district" name="district" value="{{isset($building->district)?$building->district:''}}" placeholder="-">
                      </div>
                      
                      <div class="form-group col-md-6">
                        <label for="node_id">@lang('table.city')</label>
                        <input type="text" class="form-control" id="city" name="city" value="{{isset($building->city)?$building->city:''}}" placeholder="-">
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="node_id">@lang('table.province')</label>
                        <input type="text" class="form-control" id="province" name="province" value="{{isset($building->province)?$building->province:''}}" placeholder="-">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="node_id">@lang('table.postal')</label>
                        <input type="text" class="form-control" id="postal" name="postal" value="{{isset($building->postal)?$building->postal:''}}" placeholder="-">
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="phone">@lang('table.phone')</label>
                        <input type="text" class="form-control" id="phone" name="phone" value="{{isset($building->phone)?$building->phone:''}}" placeholder="-">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="email">@lang('table.email')</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{isset($building->email)?$building->email:''}}" placeholder="-">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="website">@lang('table.website')</label>
                        <input type="text" class="form-control" id="website" name="website" value="{{isset($building->website)?$building->website:''}}" placeholder="-">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="img">@lang('table.logo')</label>
                      <div class="row my-0">
                        <div class="input-group">
                          <img src="{{ asset('/images') }}/{{ isset($building->image)?$building->image:'80x80.png'}}" id="preview" class="img-thumbnail"  width="200px"/>
                        </div>
                        <input type="hidden" name="hidden_image" value="{{ isset($building->image)?$building->image:'-' }}" />
                        <input type="file" name="image" class="file" accept="image/*">
                        <div class="input-group my-3">
                          <input type="text" class="form-control" disabled placeholder="Upload File" id="file">
                          <div class="input-group-append">
                            <button type="button" class="browse btn btn-primary">Browse</button>
                          </div><br>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                      <button type="submit" class="btn btn-primary px-5"><strong>@lang('general.save')</strong></button>
                    </div>
                </form>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>
@endsection

@section('script')
<script>
  //for image
  $(document).on("click", ".browse", function() {
    var file = $(this).parents().find(".file");
    file.trigger("click");
  });
  $('input[type="file"]').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#file").val(fileName);

    var reader = new FileReader();
    reader.onload = function(e) {
      // get loaded data and render thumbnail.
      document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });
</script>
@endsection