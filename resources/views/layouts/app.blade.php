<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Synapsis.id Warehouse</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{asset('css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/Chart.min.css')}}" rel="stylesheet" type="text/css">  
</head>
<body style="background-color:#263343">
    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light topbar fixed-top shadow d-flex flex-column align-items-center justify-content-center" style="background-color: #fff">
        <a class="sidebar-brand " href="{{ route('landing') }}" style="color: #263343; text-decoration:none">
            <span class="sidebar-brand-icon">
              <i class="fas fa-warehouse"></i>
            </span>
            <span class="sidebar-brand-text mx-2">Synapsis.id Warehouse</span>
        </a>
    </nav>
      <!-- End of Topbar -->
      
    <main class="pt-4">
        @yield('content')
    </main>
</div>
</body>
</html>
