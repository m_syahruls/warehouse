@extends('layouts.layout')

@section('style')
<link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('css/select2-boostrap.css')}}" rel="stylesheet" />

<style>
  table.dataTable th:nth-child(1) { 
    width: 8%;
    max-width: 10%;
    word-break: break-all;
    white-space: pre-line;
  }

  #img-resp{
    width: 30%;
  }

  @media (max-width: 600px) {
    #img-resp{
      width: 100%;
    }
  }

  table.dataTable td.details-control:before {
   content: '\f152';
   font-family: 'Font Awesome\ 5 Free';
   cursor: pointer;
   font-size: 22px;
   color: #55a4be;
  }
  table.dataTable tr.shown td.details-control:before {
    content: '\f150';
    color: black;
  }
  
  .pop {
    width: 30px;
    height: 30px;
    overflow: hidden;
    position: relative;
  }

  .pop img {
    height: 100%;
    min-width: 100%;
    top:0;
    left: 0;
    position: absolute;
    vertical-align:top;
    object-fit: contain;
  }
</style>
@endsection

@section('content')
@if ($message = Session::get('success'))
<div class="row" id="proBanner">
  <div class="col-12">
    <span class="d-flex align-items-center purchase-popup alert alert-success">
      <p>{{ $message }}</p>
      <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
    </span>
  </div>
</div>
@elseif ($message = Session::get('error'))
<div class="row" id="proBanner">
  <div class="col-12">
    <span class="d-flex align-items-center purchase-popup alert alert-danger">
      <p>{{ $message }}</p>
      <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
    </span>
  </div>
</div>
@endif
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">@lang('transaction.title')</h6>
  </div>
  <div class="card-body">
    <a href="{{action('TransactionController@create')}}" class="btn btn-primary btn-icon-split mb-4" style="margin-bottom: 5px;">
      <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
      </span>
      <span class="text"><strong>@lang('transaction.button_create')</strong></span>
    </a>
    @if(Auth::user())
      @if(Auth::user()->level === 'admin')
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item" role="presentation">
        <a class="nav-link active" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="true">Peruser</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link" id="permonth-tab" data-toggle="tab" href="#permonth" role="tab" aria-controls="permonth" aria-selected="false">Permonth</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link" id="perproject-tab" data-toggle="tab" href="#perproject" role="tab" aria-controls="perproject" aria-selected="false">Perproject</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      @endif
    @endif

      <div class="tab-pane fade mt-3 show active" id="users" role="tabpanel" aria-labelledby="users-tab">
        <div class="table-responsive">
          <table class="table table-bordered" id="users-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th></th>
                <th width="5%">@lang('table.photo')</th>
                <th>@lang('table.name')</th>
                <th>@lang('table.email')</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      
    @if(Auth::user())
      @if(Auth::user()->level === 'admin')
      <div class="tab-pane fade mt-3" id="permonth" role="tabpanel" aria-labelledby="permonth-tab">
        <div class ="row">
          <div class="col-xl-3 col-md-3 col">
            <input type="month" id="date-permonth" name="date-permonth" class="form-control">
          </div>
        </div>
        <div class="row">
          <div class="col-xl-12 col-md-12 col my-3">
            <div class="table-responsive">
              <table class="table table-sm table-bordered" id="permonth-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>@lang('table.date')</th>
                    <th>@lang('table.photo')</th>
                    <th>@lang('table.created_by')</th>
                    <th>@lang('table.status')</th>
                    <th>@lang('table.confirmation')</th>
                    <th>@lang('table.description')</th>
                    <th width="9%">@lang('table.action')</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="tab-pane fade mt-3" id="perproject" role="tabpanel" aria-labelledby="perproject-tab">
        <div class ="row">
          <div class="col-xl-3 col-md-3 col">
            {{-- <input type="month" id="date-perproject" name="date-perproject" class="form-control"> --}}
            <select class="form-control" id="description" name="description"></select>
          </div>
        </div>
        <div class="row">
          <div class="col-xl-12 col-md-12 col my-3">
            <div class="table-responsive">
              <table class="table table-sm table-bordered" id="perproject-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>@lang('table.date')</th>
                    <th width="5%">@lang('table.photo')</th>
                    <th>@lang('table.created_by')</th>
                    <th>@lang('table.status')</th>
                    <th>@lang('table.confirmation')</th>
                    {{-- <th>@lang('table.description')</th> --}}
                    <th width="9%">@lang('table.action')</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      @endif
    @endif

    </div>
  </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/select2.min.js')}}"></script>
<script src="{{asset('js/handlebars.min.js')}}"></script>
<script src="{{asset('datatables/sorting/natural.js')}}"></script>
<script id="details-template" type="text/x-handlebars-template">
  @verbatim
  {{!-- <div class="label label-info">Customer {{ name }}'s Purchases</div> --}}
  <table class="table table-sm details-table" id="purchases-{{id}}">
      <thead>
      <tr>
        <th>ID</th>
        <th>{{ table_date }}</th>
        <th>{{ table_status }}</th>
        <th>{{ table_confirmation }}</th>
        <th>{{ table_description }}</th>
        <th width="9%">{{ table_action }}</th>
      </tr>
      </thead>
  </table>
  @endverbatim
</script>

<script>
  var url = "{{ route('api.transaction') }}";
  var template = Handlebars.compile($("#details-template").html());
  var table = $('#users-table').DataTable({
    processing: true,
    // serverSide: true,
    // paging: false,
    // info: false,
    ajax: url,
    language: {
      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
    },
    columns: [
      {
        "className":      'details-control',
        "orderable":      false,
        "searchable":     false,
        "data":           null,
        "defaultContent": '',
      },
      {
        data: 'photo',
        name: 'photo',
        render: function(data, type, full, meta){
          return "<div href='#' class='pop'><img src='{{ asset('images/users') }}/" + data + "' width='30' class='img-thumbnail rounded-circle' alt='" + data + "'/></div>";
        },
        orderable: false,
      },
      { data: 'name', name: 'name' },
      { data: 'email', name: 'email' },
    ],
    order: [[2, 'asc']]
  });

  // Add event listener for opening and closing details
  $('#users-table tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    var tableId = 'purchases-' + row.data().id;

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {
      // Open this row
      // row.data.append({table_date: "Tanggal"});
      row.data().table_date = "{{ __('table.date') }}";
      row.data().table_description = "{{ __('table.description') }}";
      row.data().table_status = "{{ __('table.status') }}";
      row.data().table_confirmation = "{{ __('table.confirmation') }}";
      row.data().table_action = "{{ __('table.action') }}";
      row.child(template(row.data())).show();
      initTable(tableId, row.data());
      console.log(row.data());
      tr.addClass('shown');
      tr.next().find('td').addClass('no-padding bg-gray');
    }
  });

  function initTable(tableId, data) {
    tableTransaction = $('#' + tableId).DataTable({
      processing: true,
      // serverSide: true,
      // paging: false,
      ajax: data.details_url,
      language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
      columns: [
        { data: 'id_transaction', name: 'id_transaction' },
        { data: 'date', name: 'date' },
        { data: 'status', name: 'status' },
        { data: 'confirmation', name: 'confirmation' },
        { data: 'description', name: 'description' },
        {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false,
          defaultContent: ''
        }
      ],
      columnDefs: [
        { type: 'natural', targets: 1 },
      ],
      order: [[ 0, 'desc' ]],
    });
  }

  $(function() {
    $(document).on("click", '.pop', function(event) { 
      $('.imagepreview').attr('src', $(this).find('img').attr('src'));
      $('#imagemodal').modal('show');
    });
  });

  // permonth
  today = new Date();
  document.getElementById('date-permonth').valueAsDate = today;
  date_permonth = $("#date-permonth").val();
  var url_permonth = "{{ route('api.transaction') }}/permonth/"+date_permonth;
  var table_permonth = $('#permonth-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: url_permonth,
    language: {
      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
    },
    columns: [
      { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false },
      { data: 'id_transaction', name: 'id' },
      { data: 'date', name: 'date' },
      {
        data: 'create_by.photo',
        name: 'photo',
        render: function(data, type, full, meta){
          return "<a href='#' class='pop'><img src='{{ asset('images/users') }}/" + data + "' width='30' class='img-thumbnail rounded-circle' alt='" + data + "'/></a>";
        },
        orderable: false
      },
      { data: 'create_by.name', name: 'created_by' },
      { data: 'status', name: 'status' },
      { data: 'confirmation', name: 'confirmation' },
      { data: 'description', name: 'description' },
      {
          data: 'action',
          name: 'action',
          // orderable: false,
          searchable: false,
          defaultContent: ''
      }
    ],
    columnDefs: [
        { type: 'natural', targets: 1 },
        // { targets: "_all", orderable: false },
    ],
    order: [[ 1, 'desc' ]],
  });
  $("#date-permonth").change(function() {
      date_permonth = $("#date-permonth").val();
      url_permonth = "{{ route('api.transaction') }}/permonth/"+date_permonth;
      table_permonth.ajax.url(url_permonth).load();
  });

  // perproject
  description = $("#description").val();
  var url_perproject = "{{ route('api.transaction') }}/perproject/"+description;
  var table_perproject = $('#perproject-table').DataTable({
    processing: true,
    serverSide: true,
    // paging: false,
    // info: false,
    ajax: url_perproject,
    language: {
      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
    },
    columns: [
      { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false },
      { data: 'id_transaction', name: 'id' },
      { data: 'date', name: 'date' },
      {
        data: 'create_by.photo',
        name: 'photo',
        render: function(data, type, full, meta){
          return "<a href='#' class='pop'><img src='{{ asset('images/users') }}/" + data + "' width='30' class='img-thumbnail rounded-circle' alt='" + data + "'/></a>";
        },
        orderable: false
      },
      { data: 'create_by.name', name: 'created_by' },
      { data: 'status', name: 'status' },
      { data: 'confirmation', name: 'confirmation' },
      // { data: 'description', name: 'description' },
      {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false,
          defaultContent: ''
      }
    ],
    columnDefs: [
        { type: 'natural', targets: 1 },
        { targets: "_all", searchable: true },
    ],
    order: [[ 1, 'desc' ]],
  });
  // load select2
  $(document).ready(function() {
      $.ajax({
          url: "{{ route('api.project.list') }}",
          type: 'GET',
          contentType: 'application/json; charset=utf-8'
      }).then(function (response) {
        $("#description").select2({
          data: response,
          width: '100%',
          theme: "bootstrap",
          tags: true,
        });
        description = $("#description").val();
        url_perproject = "{{ route('api.transaction') }}/perproject/"+description;
        table_perproject.ajax.url(url_perproject).load();
      });
  });
  $("#description").change(function() {
      description = $("#description").val();
      url_perproject = "{{ route('api.transaction') }}/perproject/"+description;
      table_perproject.ajax.url(url_perproject).load();
  });

</script>
@endsection
