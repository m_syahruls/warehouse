@extends('layouts.layout')

@section('style')
<style>
  table.dataTable th:nth-child(1) { 
    width: 4%;
    max-width: 10%;
    word-break: break-all;
    white-space: pre-line;
  }
  table.dataTable td.details-control:before {
   content: '\f152';
   font-family: 'Font Awesome\ 5 Free';
   cursor: pointer;
   font-size: 22px;
   color: #55a4be;
  }
  table.dataTable tr.shown td.details-control:before {
    content: '\f150';
    color: black;
  }
  
  #img-resp{
    width: 30%;
  }

  @media (max-width: 600px) {
    #img-resp{
      width: 100%;
    }
  }
</style>
@endsection

@section('content')
@if ($message = Session::get('error'))
<div class="row" id="proBanner">
  <div class="col-12">
    <span class="d-flex align-items-center purchase-popup alert alert-danger">
      <p>{{ $message }}</p>
      <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
    </span>
  </div>
</div>
@endif
<div class="card shadow mb-4">
  <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">@lang('transaction.title_create')</h6>
  </div>
  <div class="card-body">
    <h4>@lang('list.cart')</h4>
    <div class="table-responsive">
      <table class="table table-bordered display responsive nowrap" id="table_cart" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('table.name')</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.qty')</th>
            <th>@lang('table.action')</th>
          </tr>
        </thead>
        <tbody>
            <tr>
              <td colspan="8" style="text-align: center">@lang('table.empty')</td>
            </tr>
        </tbody>
      </table>
    </div>

    <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
      <a href="#" class="btn btn-danger mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2" id="clearBtn">
        <span class="text">@lang('transaction.button_clear')</span>
      </a>
      <a href="{{action("TransactionController@summary")}}" class="btn btn-primary">
        <span class="text"><strong>@lang('transaction.button_summary')</strong></span>
      </a>
    </div>

    <h4>@lang('list.goodies')</h4>
    <div class="table-responsive">
      <table class="table table-bordered display responsive nowrap" id="table_goodies" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>@lang('table.photo')</th>
            <th>@lang('table.name')</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.code')</th>
            <th>@lang('table.qty')</th>
            <th>@lang('table.location')</th>
            <th>@lang('table.action')</th>
          </tr>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
            <tr>
              <td colspan="8" style="text-align: center">@lang('table.empty')</td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="goodyForm" name="goodyForm">
        <div class="modal-header">
            <h5 class="modal-title" id="modalTitle">@lang('modal.title_add_cart')</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <input type="hidden" class="form-control" id="action" name="action" disabled>
          <input type="hidden" class="form-control" id="status" name="status" disabled>
          <input type="hidden" class="form-control" id="id" name="id" disabled>
            <div class="form-group">
              <label for="name">@lang('table.name')</label>
              <div class="input-group">
                <input type="text" class="form-control" id="name" name="name" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="code">@lang('table.code')</label>
              <div class="input-group">
                <input type="text" class="form-control" id="code" name="code" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="code">@lang('table.stock')</label>
              <div class="input-group">
                <input type="number" class="form-control" id="stock" name="stock" min="0" value="1" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="code">@lang('table.qty')</label>
              <div class="input-group">
                <input type="number" class="form-control" id="qty" name="qty" min="0" value="1">
              </div>
            </div>
            <input type="hidden" name="description" id="description">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.cancel')</button>
          <button type="submit" class="btn btn-primary" id="addBtn"><strong>@lang('general.add')</strong></button>
        </div>
      </form>
    </div>
  </div>
</div>

  <!-- Modal -->
<div id="removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">@lang('modal.title_delete_cart')</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>@lang('modal.subtitle_delete_cart')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">@lang('general.no')</button>
        <button type="" class="btn btn-danger" id="removeBtn"><strong>@lang('modal.yes_remove')</strong></button>
      </div>
    </div>
  </div>
</div>

<!-- Modal for zooming image -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('datatables/sorting/natural.js')}}"></script>
<script>
  $(document).ready(function() {
    let urlCart = "{{ route('cart.index') }}";
    tableGoodies = $('#table_goodies').DataTable({
      ajax: "{{ route('cart.list') }}",
      responsive: true,
      processing:true,
      language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
      columns: [
        // { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: null, name: 'index', orderable: false },
        {
            data: 'photo',
            name: 'photo',
            render: function(data, type, full, meta){
              return "<a href='#' class='pop'><img src={{ asset('images/goodies') }}/" + data + " style='max-height:120px' class='img-thumbnail' alt='" + data + "'/></a>";
            },
            orderable: false
        },
        { data: 'name', name: 'name', },
        { data: 'code_category', name: 'code_category', },
        { data: 'code', name: 'code', },
        { data: 'qty_unit', name: 'qty_unit', },
        { data: 'location', name: 'location', },
        { data: 'action', name: 'action', },
      ],
      orderCellsTop: true,
      columnDefs: [
        { type: 'natural', targets: 3 , visible: false,},
        { type: 'natural', targets: 4 },
        { type: 'natural', targets: 5 },
        { type: 'natural', targets: 6 },
      ],
      order: [[ 4, 'asc' ]],
      initComplete: function () {
        this.api().columns([3]).every( function () {
          var column = this;
          var select = $('<select><option value=""></option></select>')
            .appendTo( $("thead tr:eq(2) th").eq(column.index()).empty() )
            .on( 'change', function () {
              var val = $.fn.dataTable.util.escapeRegex(
                  $(this).val()
              );
              column.search( val ? '^'+val+'$' : '', true, false ).draw();
          } );
          column.data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' );
          } );
        } );
      },
    });
    tableGoodies.on( 'order.dt search.dt', function () {
        tableGoodies.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $(function() {
      $(document).on("click", '.pop', function(event) { 
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#imagemodal').modal('show');
      });
    });

    tableCart = $('#table_cart').DataTable({
      ajax: urlCart,
      responsive: true,
      paging:   false,
      ordering: false,
      info:     false,
      language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'name', name: 'name', },
        { data: 'code', name: 'code', },
        { data: 'qty', name: 'qty', },
        { data: 'action', name: 'action', },
      ],
    });
  
    $(function () {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $('body').on('click', '#addGoody', function () {
        var goody_id = $(this).data('id');
        $('#addBtn').html("@lang('general.wait')");
        var url = '{{ route("cart.show", ":id") }}';
        url = url.replace(':id', goody_id);
        $.get(url, function (data) {
          $('#addModal').modal('show');
          $('#modalTitle').text("@lang('modal.title_add_cart')");
          $('#action').val("create");
          $('#status').val(data.status);
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#code').val(data.code);
          $('#stock').val(data.qty);
          $('#description').val("");
          $('#addBtn').html("@lang('general.add')");
          // document.getElementById("qty").value = data.qty;
          if(data.qty == 0){
            $('#addBtn').attr('disabled', true);
            document.getElementById("qty").disabled = true;
            document.getElementById("qty").value = 0;
            document.getElementById("qty").min = 0;
            document.getElementById("qty").max = 0;
          }else{
            $('#addBtn').attr('disabled', false);
            document.getElementById("qty").disabled = false;
            document.getElementById("qty").value = 1;
            document.getElementById("qty").min = 1;
            document.getElementById("qty").max = data.qty;
          }
          $('#qty').focus();
        })
      });
      $('body').on('click', '#editCart', function () {
        var goody_id = $(this).data('id');
        $('#addBtn').html("@lang('general.wait')");
        var url = '{{ route("cart.edit", ":id") }}';
        url = url.replace(':id', goody_id);
        $.get(url, function (data) {
          $('#addModal').modal('show');
          $('#modalTitle').text("@lang('modal.title_edit_cart')");
          $('#action').val("edit");
          $('#status').val(data.status);
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#code').val(data.code);
          $('#stock').val(data.qty);
          $('#description').val(data.cart_description);
          $('#addBtn').html("@lang('general.update')");
          document.getElementById("qty").value = data.cart_qty;
          document.getElementById("qty").min = 1;
          document.getElementById("qty").max = data.qty;
          $('#qty').focus();
        })
      });

      let validd = function(qty, status, stock, action){
        console.log(qty, status, stock, action);
        if (qty == 0){
          alert("@lang('modal.zero_cart')");
          document.getElementById("qty").value = 1;
          $('#addBtn').html("@lang('general.add')");
          if(action == "edit"){
            $('#addBtn').html("@lang('general.update')");
          }
          return false;
        }
        if(status=='single'){
          if (qty > 1) {
            alert("@lang('modal.over_cart_single')");
            document.getElementById("qty").value = 1;
            $('#addBtn').html("@lang('general.add')");
            if(action == "edit"){
              $('#addBtn').html("@lang('general.update')");
            }
            return false;
          }
        } else {
          if (qty > parseInt(stock)) {
            alert("@lang('modal.over_cart_multiple')");
            document.getElementById("qty").value = stock;
            $('#addBtn').html("@lang('general.add')");
            if(action == "edit"){
              $('#addBtn').html("@lang('general.update')");
            }
            return false;
          }
        }
        return true;
      }

      $('#addBtn').click(function (e) {
        e.preventDefault();
        $(this).html("@lang('general.sending')");

        action = $('#action').val();
        status = $('#status').val();
        id = $('#id').val();
        code = $('#code').val();
        stock = $('#stock').val();
        qty = $('#qty').val();
        description = $('#description').val();

        if(!validd(qty, status, stock, action)) return false;
        
        let url = "{{ route('cart.store') }}";
        let type = "POST";
        if(action == "edit"){
          url = '{{ route("cart.update", ":id") }}';
          url = url.replace(':id', id);
          type = "PUT";
        }
        
        $.ajax({
          data:{
            "_token": "{{ csrf_token() }}",
            "_method": type,
            id:id,
            code:code,
            qty:qty,
            description:description,
          },
          url: url,
          type: type,
          dataType: 'json',
          success: function (data) {
              $('#goodyForm').trigger("reset");
              $('#addModal').modal('hide');
              $('#addBtn').html("@lang('general.add')");
              console.log(data);
              tableCart.ajax.url(urlCart).load();
          },
          error: function (data) {
              console.log('Error:', data);
              $('#addBtn').html("@lang('general.add')");
          }
        });
      });
    });

    $('#clearBtn').click(function (e) {
      $.ajax({
        url: "{{ route('cart.clear') }}",
        type: 'GET',
        dataType: 'json',
        success: function (data) {
          console.log(data);
          tableCart.ajax.url(urlCart).load();
        },
        error: function (data) {
          console.log('Error:', data);
        }
      });
    });

    $('#removeModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var id = button.data('id'); // Extract info from data-* attributes
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      var form = document.getElementById("deleteForm");
      url = '{{ route("cart.destroy", ":id") }}';
      url = url.replace(':id', id);
      $('#removeBtn').click(function (e) {
        $.ajax({
            data:{
              "_token": "{{ csrf_token() }}",
              "_method": "DELETE",
              id:id,
            },
            url: url,
            type: "DELETE",
            dataType: 'json',
            success: function (data) {
                $('#removeModal').modal('hide');
                console.log(data);
                tableCart.ajax.url(urlCart).load();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
      });
    })
})
</script>
@endsection