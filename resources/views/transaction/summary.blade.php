@extends('layouts.layout')

@section('style')
<link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
<link href="{{ asset('css/select2-boostrap.css')}}" rel="stylesheet" />

<style>
    .select2-selection__arrow b{
        display:none !important;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col-lg-10">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h5 class="m-0 font-weight-bold text-primary">@lang('transaction.title_summary')</h5>
            </div>
            <div class="card-body">
                @if (count($errors) > 0)
                    <p class="card-description"> 
                        <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        </div>
                    </p>
                @endif
                <form action="{{action('TransactionController@store')}}" method="POST" id="transaction-form">
                    @csrf
                    <div class="form-group">
                        <label for="id_transaction">@lang('table.id')</label>
                        <input type="text" class="form-control" name="id_transaction" id="id_transaction" readonly placeholder="ID Transaksi">
                    </div>
                    <div class="form-group">
                        <label for="date">@lang('table.date')</label>
                        <input type="date" class="form-control" name="date" id="date" value="" placeholder="Tanggal Sekarang">
                    </div>
                    <div class="form-group">
                        <label for="created_by">@lang('table.created_by')</label>
                        <input type="hidden" name="created_by" id="created_by" value="{{isset(Auth::user()->id)? Auth::user()->id :""}}">
                        <input type="text" class="form-control" name="user_name" id="user_name" readonly disabled value="{{isset(Auth::user()->name)? Auth::user()->name :""}}" placeholder="Nama User">
                    </div>
                    <div class="form-group">
                        <label for="name">@lang('list.goodies')</label>
                        <div class="table-responsive">
                            <table class="table table-bordered display responsive nowrap" id="table" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('table.name')</th>
                                        <th>@lang('table.code')</th>
                                        <th>@lang('table.qty')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="5" style="text-align: center">@lang('table.empty')</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">@lang('table.description')</label>
                        <div class="input-group">
                            {{-- <textarea type="text" class="form-control" id="description" name="description" cols="30" rows="4" placeholder="Write description..." autofocus></textarea> --}}
                            <select class="form-control" id="description" name="description"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">@lang('table.status')</label>
                        <div class="input-group">
                            <select id="status" name="status" class="form-control">
                                <option value="request">Request</option>
                                <option value="borrow">Borrow</option>
                                <option value="facility">Facility</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col">
                            <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                                <a href="{{ route('transaction.create') }}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.back')</a>
                                {{-- <button type="submit" class="btn btn-primary"><strong>@lang('transaction.button_create')</strong></button> --}}
                                <button type="button" class="btn btn-primary" id="btn-submit"><strong>@lang('transaction.button_create')</strong></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>
@endsection

@section('script')
<script src="{{asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
            url: "{{ route('api.project.list')}}",
            type: 'GET',
            contentType: 'application/json; charset=utf-8'
        }).then(function (response) {
            $("#description").select2({
                allowClear: true,
                placeholder: {
                    id: "-1",
                    text:"Select a Project",
                    selected:'selected'
                },
                minimumInputLength: 3,
                data: response,
                width: '100%',
                theme: "bootstrap",
                tags: true,
            });
            $('#description').select2('val', '-1');
        });
    });
</script>
<script>
    $(document).ready(function() {
        document.getElementById('date').valueAsDate = new Date();
        let urlCart = "{{ route('cart.index') }}";
        tableCart = $('#table').DataTable({
            ajax: urlCart,
            responsive: true,
            paging:   false,
            ordering: false,
            info:     false,
            searching: false,
            processing:true,
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'name', name: 'name', },
                { data: 'code', name: 'code', },
                { data: 'qty', name: 'qty', },
            ],
        });

        var checkId = function(){
            $.ajax({
                url: "{{ route('transaction.checkid') }}",
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var idInput = document.getElementById("id_transaction");
                    if(data.stock){
                        var idDb = data.stock.id_transaction;
                        if (idDb.match(/SK.*/)){
                            let addId = parseInt(idDb.split('K')[1])+1;
                            idInput.value = 'SK'+addId;
                        }
                    }else{
                        idInput.value = 'SK1';
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
        checkId();

        $(document).on('click','#btn-submit',function(){
            $('#btn-submit').html("@lang('general.sending')");
            $('#btn-submit').attr('disabled', true);
            checkId();
            setTimeout(function(){
                $("#transaction-form").submit();
            }, 1000);
        });
    })
</script>
@endsection