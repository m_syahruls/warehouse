@extends('layouts.layout')

@section('content')
@if ($message = Session::get('success'))
<div class="row" id="proBanner">
  <div class="col-12">
    <span class="d-flex align-items-center purchase-popup alert alert-success">
      <p>{{ $message }}</p>
      <i class="mdi mdi-close ml-auto" id="bannerClose"></i>
    </span>
  </div>
</div>
@endif

<div class="row">
  <div class="col"></div>
  <div class="col-lg-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">@lang('profile.title')</h5>
      </div>
      <div class="card-body">
        <div class="form-group">
          <label for="photo">@lang('table.photo')</label>
          <div class="input-group">
            <img src="{{ asset('images/users') }}/{{ $user->photo }}" id="preview" class="img-thumbnail" width="200px"/>
          </div>
        </div>
        <div class="form-group">
          <label for="name">@lang('table.name')</label>
          <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autocomplete="name" disabled>
        </div>
        <div class="form-group">
          <label for="username">@lang('table.username')</label>
          <input id="username" type="text" class="form-control" name="username" value="{{ $user->username }}" required autocomplete="username" disabled>
        </div>
        <div class="form-group">
          <label for="email">@lang('table.email')</label>
          <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required autocomplete="email" disabled>
        </div>
        <div class="form-group">
          <label for="password-confirm">@lang('table.level')</label>
          <input id="level" type="text" class="form-control" name="level" value="{{ ucfirst($user->level) }}" required autocomplete="level" disabled>
        </div>
        <div class="row mb-1">
          <div class="col">
            <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
              <a type="button" href="{{action('ProfileController@edit', $user->id)}}" class="btn btn-primary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('profile.button_edit')</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>  

@endsection