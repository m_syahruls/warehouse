@extends('layouts.layout')

@section('content')
<div class="row">
  <div class="col"></div>
  <div class="col-lg-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">@lang('profile.title_password')</h5>
      </div>
      <div class="card-body">
        @if ($message = Session::get('error'))
          <p class="card-description"> 
            <div class="alert alert-danger">
              <p>{{ $message }}</p>
            </div>
          </p>
        @endif
        <form action="{{action('ProfileController@update', $user->id)}}" method="POST">
        @csrf
        {{ method_field('PUT') }}
          <div class="form-group">
            <label for="old_password">@lang('table.password')</label>
            <input id="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required autocomplete="new-password" autofocus>
            @error('old_password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="password">@lang('table.password_new')</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="password-confirm">@lang('table.password_confirm')</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
          </div>
          <div class="row mb-1">
            <div class="col">
              <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                <a type="button" href="{{ isset(Auth::user()->name)? action('ProfileController@index', 'u='.Auth::user()->username) :"#" }}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.cancel')</a>
                <button type="submit" class="btn btn-primary"><strong>@lang('profile.button_password')</strong></button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col"></div>
</div>
@endsection