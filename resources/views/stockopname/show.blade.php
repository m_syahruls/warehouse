@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col"></div>
    <div class="col-lg-10">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h5 class="m-0 font-weight-bold text-primary">@lang('stockopname.title_detail')</h5>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="id_transaction">@lang('table.id')</label>
                    <input type="text" class="form-control" name="id_transaction" id="id_transaction" readonly disabled value="{{$stock->id_transaction}}">
                </div>
                <div class="form-group">
                    <label for="name">@lang('table.name_transaction')</label>
                    <input type="text" class="form-control" name="name" id="name" readonly disabled value="{{$stock->name}}">
                </div>
                <div class="form-group">
                    <label for="date">@lang('table.date')</label>
                    <input type="text" class="form-control" name="date" id="date" readonly disabled value="{{$stock->date}}">
                </div>
                <div class="form-group">
                    <label for="user_name">@lang('table.created_by')</label>
                    <input type="text" class="form-control" name="user_name" id="user_name" readonly disabled value="{{$stock->create_by->name}}">
                </div>
                <div class="form-group">
                    <label for="name">@lang('list.goodies')</label>
                    <div class="table-responsive">
                        <table class="table table-bordered display responsive nowrap" id="table" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('table.name')</th>
                                    <th>@lang('table.code')</th>
                                    <th>@lang('table.qty')</th>
                                    <th>@lang('table.description')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                    <tr>
                                        <td width="4%">{{$loop->iteration}}</td>
                                        <td>{{$item->goody->name}}</td>
                                        <td>{{$item->goody->code}}</td>
                                        <td>{{$item->qty}} {{$item->goody->unit->name}}</td>
                                        <td>{{$item->description}}</td>
                                    </tr>
                                @empty
                                    {{-- <tr>
                                        <td colspan="5" style="text-align: center">@lang('table.empty')</td>
                                    </tr> --}}
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">@lang('table.description')</label>
                    <div class="input-group">
                        <textarea readonly class="form-control" id="description" name="description" cols="30" rows="4">{{$stock->description}}</textarea>
                    </div>
                </div>
                <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                    <a type="button" href="{{action('StockOpnameController@index')}}" class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">@lang('general.back')</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col"></div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            responsive: true,
            paging:   false,
            ordering: false,
            info:     false,
            searching: false,
        });
    })
</script>
@endsection