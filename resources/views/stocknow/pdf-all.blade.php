@php
  set_time_limit(0);
  ini_set("memory_limit",-1);
  ini_set('max_execution_time', 0);
@endphp

<html>
<head>
  <style type="text/css">
    p {
      font-size: 9pt;
    }
    table tr td, table tr th {
      font-size: 9pt;
      border: 1px solid black;
      border-collapse: collapse;
    }
    .borderless td, .borderless th {
        border: none;
    }
    th, td {
      padding: 5px;
    }
  </style>
</head>
<body>
  <center>
    <img src="{{ public_path('/images') }}/{{$building->image}}" style="height: 80px;"><br>
    <p>
      <big>{{$building->name}}</big><br>
      <small>
        {{$building->address}}, {{$building->district}}, {{$building->city}}, {{$building->province}}, {{$building->postal}},<br>
        Telephone {{$building->phone}}, Email {{$building->email}}, Website {{$building->website}}.
      </small>
    </p>
    <hr>
    <p>
      <strong>Warehouse Stock Now</strong><br>
      <small>at {{\Carbon\Carbon::now()->format('d-m-Y H:i:s')}}</small>
    </p>
  </center>

  <table width="100%" cellspacing="0" class="table table-sm table-bordered" border="1">
    <thead>
      <tr>
        <th width="5%">#</th>
        {{-- <th width="12%">Photo</th> --}}
        <th>Name</th>
        <th width="8%">Code</th>
        <th width="8%">Qty</th>
        <th>Location</th>
      </tr>
    </thead>
    <tbody>
      @forelse($goodies as $goody)
        <tr>
          <td>{{$loop->iteration}}</td>
          {{-- <td>
            <img src="{{ public_path('/images/goodies') }}/{{$goody->photo}}" class="img-thumbnail" style="width: 40px;">
          </td> --}}
          <td>{{$goody->name}}</td>
          <td><nobr>{{$goody->code}}</nobr></td>
          <td><nobr>{{$goody->qty}} {{$goody->unit->name}}</nobr></td>
          <td>{{$goody->location}}</td>
        </tr>
      @empty
        <tr>
          <td colspan="8" style="text-align: center">Empty Data</td>
        </tr>
      @endforelse
    </tbody>
  </table>

</body>
</html>