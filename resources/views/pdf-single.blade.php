@php
  set_time_limit(0);
  ini_set("memory_limit",-1);
  ini_set('max_execution_time', 0);
@endphp

<html>
<head>
  <style type="text/css">
    p {
      font-size: 9pt;
    }
    table tr td, table tr th {
      font-size: 9pt;
      border: 1px solid black;
      border-collapse: collapse;
    }
    .borderless td, .borderless th {
        border: none;
    }
    th, td {
      padding: 5px;
    }
  </style>
</head>
<body>
  <center>
    <img src="{{ public_path('/images') }}/{{$building->image}}" style="height: 80px;"><br>
    <p>
      <big>{{$building->name}}</big><br>
      <small>
        {{$building->address}}, {{$building->district}}, {{$building->city}}, {{$building->province}}, {{$building->postal}},<br>
        Telephone {{$building->phone}}, Email {{$building->email}}, Website {{$building->website}}.
      </small>
    </p>
    <hr>
    <p><strong>Warehouse {{$name}} Form</strong></p>
  </center>

  <table class='table table-sm borderless'>
    <tr>
      <td width="15%">Transaction ID</td>
      <td width="3%">:</td>
      <td>{{ $stock->id_transaction }}</td>
    </tr>
    @if ($type == 'stockin')
      <tr>
        <td>Stock Out ID</td>
        <td>:</td>
        <td>@if ($stock->stockout){{$stock->stockout->id_transaction}} @else-@endif</td>
      </tr>
    @endif
    <tr>
      <td>Date</td>
      <td>:</td>
      <td>{{ $stock->date }}</td>
    </tr>
    <tr>
      <td>Created by</td>
      <td>:</td>
      <td>{{ $stock->create_by->name }}</td>
    </tr>
    @if ($type == 'stockopname')
      <tr>
        <td>Transaction Name</td>
        <td>:</td>
        <td>{{ $stock->name }}</td>
      </tr>
    @endif
    @if ($type != 'stockopname')
      <tr>
        <td>Status</td>
        <td>:</td>
        <td>{{ ucfirst($stock->status) }}</td>
      </tr>
    @endif
    @if ($type == 'stockout')
      <tr>
        <td>Confirmation</td>
        <td>:</td>
        <td>{{ ucfirst($stock->confirmation) }}</td>
      </tr>
    @endif
    <tr>
      <td>Description</td>
      <td>:</td>
      <td>{{ $stock->description }}</td>
    </tr>
  </table>
  <br>
  <table width="100%" cellspacing="0" class="table table-sm table-bordered" border="1">
    <thead>
      <tr>
        <th width="5%">#</th>
        <th>Name</th>
        <th width="8%">Code</th>
        <th width="8%">Qty</th>
        <th>Desciption</th>
      </tr>
    </thead>
    <tbody>
      @forelse($stock->items as $item)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$item->goody->name}}</td>
          <td>{{$item->goody->code}}</td>
          <td>{{$item->qty}} {{$item->goody->unit->name}}</td>
          <td>{{$item->description}}
            @if ($type == 'stockout')
              @if ($item->description),@endif
              @if ($stock->status == 'borrow' && ($stock->confirmation == 'done' || $stock->confirmation == 'returned'))
                  @if (count($item->stockitem))
                      Returned {{$item->returned}}
                  @else
                      Not Returned
                  @endif
              @endif
            @endif
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="8" style="text-align: center">Empty Data</td>
        </tr>
      @endforelse
    </tbody>
  </table>

</body>
</html>