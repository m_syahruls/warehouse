<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockIn extends Model
{
    protected $table = 'stock_in';

    protected $fillable = [
        'id_transaction', 'out_id', 'status', 'date', 'description', 'created_by'
    ];

    public function create_by()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    // checking if there any StockIn attach
    public function stockout()
    {
    	return $this->belongsTo('App\StockOut', 'out_id', 'id');
    }

    public function items()
    {
        return $this->hasMany('App\StockInItem', 'stock_id', 'id');
    }
}
