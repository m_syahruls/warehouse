<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOutItem extends Model
{
    protected $table = 'stock_out_item';

    protected $fillable = [
        'stock_id', 'goody_id', 'qty', 'description'
    ];

    public function stock()
    {
    	return $this->belongsTo('App\StockOut', 'stock_id', 'id');
    }

    // checking if there any StockInItem attach
    public function stockitem()
    {
    	return $this->hasMany('App\StockInItem', 'stockout_item_id', 'id');
    }

    public function goody()
    {
    	return $this->belongsTo('App\Goody', 'goody_id', 'id');
    }
}
