<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOut extends Model
{
    protected $table = 'stock_out';

    protected $fillable = [
        'id_transaction', 'status', 'status_transaction', 'date', 'description', 'created_by', 'updated_by'
    ];

    public function create_by()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function update_by()
    {
    	return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    // checking if there any StockIn attach
    public function stockin()
    {
        return $this->hasMany('App\StockIn', 'out_id', 'id');
    }

    public function items()
    {
        return $this->hasMany('App\StockOutItem', 'stock_id', 'id');
    }
}
