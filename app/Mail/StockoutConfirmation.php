<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Stockout;

class StockoutConfirmation extends Mailable
{
    use Queueable, SerializesModels;
    public $stock;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Stockout $stock)
    {
        $this->stock = $stock;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@warehouse.com')
                    ->markdown('mail.stockout-confirmation')
                    ->with([
                        'link' => url('home')
                    ]);
    }
}
