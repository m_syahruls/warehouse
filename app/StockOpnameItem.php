<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOpnameItem extends Model
{
    protected $table = 'stock_opname_item';

    protected $fillable = [
        'stock_id', 'goody_id', 'qty', 'description'
    ];

    public function stock()
    {
    	return $this->belongsTo('App\StockOpname', 'stock_id', 'id');
    }

    public function goody()
    {
    	return $this->belongsTo('App\Goody', 'goody_id', 'id');
    }
}
