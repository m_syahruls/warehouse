<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOpname extends Model
{
    protected $table = 'stock_opname';

    protected $fillable = [
        'id_transaction', 'name', 'date', 'description', 'created_by'
    ];

    public function create_by()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function items()
    {
        return $this->hasMany('App\StockOpnameItem', 'stock_id', 'id');
    }
}
