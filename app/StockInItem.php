<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockInItem extends Model
{
    protected $table = 'stock_in_item';

    protected $fillable = [
        'stock_id', 'goody_id', 'qty', 'stockout_item_id'
    ];

    public function stock()
    {
    	return $this->belongsTo('App\StockIn', 'stock_id', 'id');
    }

    // checking if there any StockOutItem attach
    public function stockitem()
    {
        return $this->belongsTo('App\StockOutItem', 'stockout_item_id', 'id');
    }

    public function goody()
    {
    	return $this->belongsTo('App\Goody', 'goody_id', 'id');
    }
}
