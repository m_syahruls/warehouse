<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = ['name', 'address', 'district', 'city', 'province', 'postal', 'phone', 'email', 'website', 'image',];
}
