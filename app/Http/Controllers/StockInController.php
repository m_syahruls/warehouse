<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockIn;
use App\StockInItem;
use App\StockOut;
use App\StockOutItem;
use App\Goody;
use App\Category;
use App\Unit;
use App\Building;

use Cart;
use Carbon\Carbon;
use PDF;

class StockInController extends Controller
{
    public function index()
    {
        $stocks = StockIn::all()->sortByDesc('created_at');
        return view('stockin.index')->with(compact('stocks'));
    }

    public function create()
    {
        $stocks = StockOut::where('confirmation', '=', 'done')
                    ->where('status', '=', 'borrow')
                    ->get()->sortBy('created_at');
        return view('stockin.create',compact('stocks'));
    }

    public function summary(Request $request)
    {
        if(Cart::isEmpty()){
            return redirect()->route('stockin.create')
            ->with('error','Cart must be fill.');
        }

        $stock="";
        $status = $request->transaction_status;
        if ($status != 'purchase') {
            $stock = StockOut::where('id', '=', $status)->firstOrFail();
        }
        return view('stockin.summary')->with(compact('stock'));
    }

    public function checkId()
    {
        $stock = StockIn::latest()->first();
        return response()->json(compact('stock'));
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'created_by'           => 'required',
            'id_transaction'         => 'required|unique:stock_in',
        ]);

        $date = Carbon::parse($request->date)->format('Y-m-d');// for date column

        $stock = new StockIn;
        $stock->id_transaction = $request->id_transaction;
        if($request->stockout_id){
            $stock->out_id = $request->stockout_id;
            $stock->status = "return";
        }else{
            $stock->status = "purchase";
        }
        $stock->date = $date;
        $stock->description = $request->description;
        $stock->created_by = $request->created_by;
        $stock->save();

        $stockin = StockIn::where('id_transaction', '=', $request->id_transaction)->firstOrFail();// for get id in stock_opname

        $cartCollection = Cart::getContent();
        foreach ($cartCollection as $collection){
            $item = new StockInItem;
            $item->stock_id = $stockin->id;
            $item->goody_id = $collection->id;
            $item->qty = $collection->quantity;
            // checking if the transaction is Returned Item
            if($request->stockout_id){
                $stockout = StockOut::where('id', '=', $request->stockout_id)->firstOrFail();
                $itemout = StockOutItem::where('stock_id', '=', $request->stockout_id)->where('goody_id', '=', $collection->id)->firstOrFail();
                $item->stockout_item_id = $itemout->id;
                // checking if return same amount when borrowed
                $total_returned = 0;
                if ($item->qty != $itemout->qty_confirm) {
                    $qtystockin = StockInItem::where('stockout_item_id', '=', $itemout->id)->where('goody_id', '=', $collection->id)->get();// for get id in stock_in
                    // checking amount qty returned
                    $total_returned += $item->qty;
                    foreach($qtystockin as $itemin){
                        $total_returned += $itemin->qty;
                    }
                }
            }
            $item->save();

            $goody = Goody::where('id', '=', $collection->id)->firstOrFail();
            $goody->qty += $collection->quantity;
            $goody->save();
        }

        if ($request->stockout_id) {
            // checking if total return same amount when borrowed
            $item_debt = 0;
            foreach($stockout->items as $item){
                $item->returned = 0;
                foreach($item->stockitem as $item_return){
                    $item->returned += $item_return->qty;
                }
                if ($item->returned != $item->qty_confirm) {
                    $item_debt += 1;
                }
            }
            if ($item_debt != 0) {
                $stockout->confirmation = "done";
                $stockout->save();
            } else {
                $stockout->confirmation = "returned";
                $stockout->save();
            }
        }

        Cart::clear();

        return redirect()->route('stockin.index')
                ->with('Success','Stock In created Successfully.');
    }

    public function show($id)
    {
        $stock = StockIn::where('id', '=', $id)->firstOrFail();
        return view('stockin.show')->with(compact('stock'));
    }

    public function destroy($id)
    {
        $stock = StockIn::where('id', '=', $id)->firstOrFail();
        $stock->delete();
        return redirect()->route('stockin.index')
            ->with('success','Stock In deleted successfully.');
    }

    public function exportSinglePdf($id)
    {
        $stock = StockIn::where('id', '=', $id)->firstOrFail();
        $building = Building::latest()->first();
        if(empty($building)){
            return redirect()->action('StockInController@index')
                ->with('error','Fill Building Information first, ask Admin for further information.');
        }
        $name = "Stock In";
        $type = "stockin";
        $pdf = PDF::loadview('pdf-single', compact('stock', 'name', 'type', 'building'))->setPaper('a4', 'potrait');
        $filename = 'Stock In - ' . $stock->id_transaction . '.pdf';
        return $pdf->download($filename);
    }
}
