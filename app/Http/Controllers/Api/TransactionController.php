<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\StockOut;

use Carbon\Carbon;
use DataTables;

class TransactionController extends Controller
{
    public function getUser()
    {
        $users = User::select();
        return Datatables::of($users)
            ->addColumn('details_url', function($user) {
                return route('api.transaction_single', $user->id);
            })->make(true);
    }

    public function getSingleUser($id)
    {
        $transactions = User::findOrFail($id)->transactions->sortByDesc('created_at');

        foreach($transactions as $transaction){
            $transaction->description = \Illuminate\Support\Str::limit($transaction->description, 40, '...');
            $transaction->status = ucfirst($transaction->status);
            $transaction->confirmation = ucfirst($transaction->confirmation);
        }

        return Datatables::of($transactions)
            ->addColumn('action', function($data){
                return $this->getActionColumn($data);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function getActionColumn($data)
    {
        $printUrl = action('StockOutController@exportSinglePdf', $data->id);
        $showUrl = action('TransactionController@show', $data->id);
        $btn = ' <a href="'.$printUrl.'" class="btn btn-success btn-circle btn-sm mb-1"><i class="fas fa-print"></i></a> ';
        $btn = $btn.' <a href="'.$showUrl.'" class="btn btn-primary btn-circle btn-sm mb-1"><i class="fas fa-eye"></i></a> ';
        return $btn;
    }

    public function getPermonth($date)
    {
        $month = Carbon::parse($date)->format('Y-m');
        $stocks = StockOut::where('date', 'like', '%' . $month . '%')->with('create_by')->get()->sortByDesc('created_at');
        foreach ($stocks as $stock) {
            $stock->create_by->name = \Illuminate\Support\Str::limit($stock->create_by->name , 20, '...');
            $stock->status = ucfirst($stock->status);
            $stock->confirmation = ucfirst($stock->confirmation);
            isset($stock->description)?$stock->description = \Illuminate\Support\Str::limit($stock->description, 30, '...'):$stock->description = "-";
        }
        return Datatables::of($stocks)
            ->addColumn('action', function($data){
                return $this->getActionColumn($data);
            })
            ->rawColumns(['action'])
            ->addIndexColumn()->make(true);
    }

    public function getPerproject($project)
    {
        $stocks = StockOut::where('description', 'like', '%' . $project . '%')->get()->sortByDesc('created_at');
        foreach ($stocks as $stock) {
            $stock->create_by->name = \Illuminate\Support\Str::limit($stock->create_by->name , 20, '...');
            $stock->status = ucfirst($stock->status);
            $stock->confirmation = ucfirst($stock->confirmation);
            isset($stock->description)?$stock->description = \Illuminate\Support\Str::limit($stock->description, 30, '...'):$stock->description = "-";
        }
        return Datatables::of($stocks)
            ->addColumn('action', function($data){
                return $this->getActionColumn($data);
            })
            ->rawColumns(['action'])
            ->addIndexColumn()->make(true);
    }
}
