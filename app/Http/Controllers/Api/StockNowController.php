<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Goody;
use DataTables;

class StockNowController extends Controller
{
    public function list()
    {
        $goodies = Goody::orderBy('code')->get();
        foreach($goodies as $goody){
            $goody->qty_unit = $goody->qty." ".$goody->unit->name;
            $goody->code_category = $goody->category->code;
        }

        return Datatables::of($goodies)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return $this->getActionColumn($data);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function getActionColumn($data)
    {
        $showUrl = action('StockNowController@show', $data->id);
        $btn = ' <a href="'.$showUrl.'" class="btn btn-primary btn-circle btn-sm mb-1"><i class="fas fa-eye"></i></a>';
        return $btn;
    }
}
