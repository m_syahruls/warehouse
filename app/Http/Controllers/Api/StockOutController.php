<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Goody;
use App\StockOut;

use DataTables;

class StockOutController extends Controller
{
    public function listProject()
    {
        $projects = StockOut::groupBy('description')->pluck('description');
        $list=[];
        foreach($projects as $project){
            if($project!=null)
                $list[]=['id'=>$project, 'text'=>$project];
        }
        return response()->json($list);
    }
}
