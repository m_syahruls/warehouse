<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Goody;
use App\Category;
use App\Unit;

use DataTables;

class GoodyController extends Controller
{
    public function list()
    {
        $goodies = Goody::with('unit')->orderBy('code')->get();
        foreach($goodies as $goody){
            $goody->code_category = $goody->category->code;
        }

        return Datatables::of($goodies)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return $this->getActionColumn($data);
            })
            ->rawColumns(['photo_html', 'action'])
            ->make(true);
    }

    protected function getActionColumn($data)
    {
        $showUrl = action('GoodyController@show', $data->id);
        $editUrl = action('GoodyController@edit', $data->id);
        $btn = ' <a href="'.$showUrl.'" class="btn btn-primary btn-circle btn-sm mb-1"><i class="fas fa-eye"></i></a> ';
        $btn = $btn.' <a href="'.$editUrl.'" class="btn btn-warning btn-circle btn-sm mb-1"><i class="fas fa-edit"></i></a> ';
        $btn = $btn.'<a href="#" class="btn btn-danger btn-circle btn-sm mb-1" data-toggle="modal" data-target="#deleteModal" 
        data-id="'.$data->id.'"><i class="fas fa-trash"></i></a> ';
        return $btn;
    }

    public function checkCategory($id)
    {
        $category = Category::find($id);
        $goody = Goody::where('code', 'like', $category->code.'%')->where('category_id', '=', $id)->latest()->first();
        return response()->json(compact('category','goody'));
    }

    public function listCategory()
    {
        $categories = Category::orderBy('code')->get();
        $goody_last = Goody::latest()->first();
        $list=[];
        foreach($categories as $category){
            if($goody_last){
                if($category->id == $goody_last->category_id)
                    $list[]=['id'=>$category->id, 'text'=>$category->code.' | '.$category->name, 'selected'=>'selected'];
                else
                    $list[]=['id'=>$category->id, 'text'=>$category->code.' | '.$category->name];
            }
            else
                $list[]=['id'=>$category->id, 'text'=>$category->code.' | '.$category->name];
        }
        return response()->json($list);
    }

    public function listCategorySelected($id)
    {
        $categories = Category::orderBy('code')->get();
        $list=[];
        foreach($categories as $category){
            if($category->id==$id)
                $list[]=['id'=>$category->id, 'text'=>$category->code.' | '.$category->name, 'selected'=>'selected'];
            else
                $list[]=['id'=>$category->id, 'text'=>$category->code.' | '.$category->name];
        }
        return response()->json($list);
    }

    public function listUnit()
    {
        $units = Unit::orderBy('name')->get();
        $goody_last = Goody::latest()->first();
        $list=[];
        foreach($units as $unit){
            if($goody_last){
                if($unit->id == $goody_last->unit_id)
                    $list[]=['id'=>$unit->id, 'text'=>$unit->name, 'selected'=>'selected'];
                else
                    $list[]=['id'=>$unit->id, 'text'=>$unit->name];
            }
            else
                $list[]=['id'=>$unit->id, 'text'=>$unit->name];
        }
        return response()->json($list);
    }

    public function listUnitSelected($id)
    {
        $units = Unit::orderBy('name')->get();
        $list=[];
        foreach($units as $unit){
            if($unit->id==$id)
                $list[]=['id'=>$unit->id, 'text'=>$unit->name, 'selected'=>'selected'];
            else
                $list[]=['id'=>$unit->id, 'text'=>$unit->name];
        }
        return response()->json($list);
    }
}
