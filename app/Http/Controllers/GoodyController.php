<?php

namespace App\Http\Controllers;

use App\Goody;
use Illuminate\Http\Request;
use App\Category;
use App\Unit;
use File;

class GoodyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('goody.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $units = Unit::all();
        return view('goody.create',compact('categories', 'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['code'=>$request->category_code." ".$request->code]);
        $request->validate([
            'category_id'   => 'required',
            'code'          => 'required|unique:goodies',
            'name'          => 'required',
            'unit'          => 'required',
            'status'        => 'required',
            'photo'         => 'required|image|max:2048',
        ]);

        $photo = $request->file('photo');
        $new_name = rand() . '.' . $photo->getClientOriginalExtension();
        $photo->move(public_path('images/goodies'), $new_name);

        $goody = new Goody;
        $goody->category_id = $request->category_id;
        $goody->code = $request->code;
        $goody->name = $request->name;
        $goody->photo = $new_name;
        $goody->qty = 0;
        $goody->unit_id = $request->unit;
        $goody->location = $request->location;
        $goody->status = $request->status;
        $goody->description = $request->description;
        $goody->save();
        return redirect()->action('GoodyController@index')
            ->with('success','Goody created successfully.');
    }

    public function storeStockin(Request $request)
    {
        $request->merge(['code'=>$request->category_code." ".$request->code]);
        $request->validate([
            'category_id'   => 'required',
            'code'          => 'required|unique:goodies',
            'name'          => 'required',
            'unit'          => 'required',
            'status'        => 'required',
            'photo'         => 'required|image|max:2048',
        ]);

        $photo = $request->file('photo');
        $new_name = rand() . '.' . $photo->getClientOriginalExtension();
        $photo->move(public_path('images/goodies'), $new_name);

        $goody = new Goody;
        $goody->category_id = $request->category_id;
        $goody->code = $request->code;
        $goody->name = $request->name;
        $goody->photo = $new_name;
        $goody->qty = 0;
        $goody->unit_id = $request->unit;
        $goody->location = $request->location;
        $goody->status = $request->status;
        $goody->description = $request->description;
        $goody->save();
        return response()->json("New Goody Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Goody  $goody
     * @return \Illuminate\Http\Response
     */
    public function show(Goody $goody)
    {
        return view('goody.show')->with(compact('goody'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Goody  $goody
     * @return \Illuminate\Http\Response
     */
    public function edit(Goody $goody)
    {
        $categories = Category::all();
        $units = Unit::all();
        return view('goody.edit')->with(compact('goody', 'categories', 'units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Goody  $goody
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Goody $goody)
    {
        $request->merge(['code'=>$request->category_code." ".$request->code]);
        if($request->code != $goody->code){
            $validate = $request->validate([
                'code'          => 'required|unique:goodies',
            ]);
        }
        $request->validate([
            'category_id'   => 'required',
            'code'          => 'required',
            'name'          => 'required',
            'unit'          => 'required',
        ]);

        $photo_name = $request->hidden_photo;
        $photo = $request->file('photo');
        if($photo != ''){
            $request->validate([
                'photo'     => 'required|image|max:2048',
            ]);
            $photo_path = "images/goodies/".$photo_name;  // Value is not URL but directory file path
            if(File::exists($photo_path)) {
                File::delete($photo_path);
            }
            $photo_name = rand() . '.' . $photo->getClientOriginalExtension();
            $photo->move(public_path('images/goodies'), $photo_name);
        }

        $goody->category_id = $request->category_id;
        $goody->code = $request->code;
        $goody->name = $request->name;
        $goody->photo = $photo_name;
        $goody->unit_id = $request->unit;
        $goody->location = $request->location;
        $goody->status = $request->status;
        $goody->description = $request->description;
        $goody->update();
        return redirect()->action('GoodyController@index')
            ->with('success','Goody updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goody  $goody
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goody $goody)
    {
        $photo_path = "images/goodies/".$goody->photo;  // Value is not URL but directory file path
        if(File::exists($photo_path)) {
            File::delete($photo_path);
        }
        $goody->delete();
        return redirect()->action('GoodyController@index')
            ->with('success','Goody deleted successfully.');
    }
}
