<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\StockOut;
use App\User;

use App\Mail\StockoutUpdated;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $stocks = StockOut::where('created_by', '=', auth()->user()->id)->get()->sortByDesc('created_at');
        return view('home.user.index')->with(compact('stocks'));
    }

    public function show($id){
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        return view('home.user.show')->with(compact('stock'));
    }

    public function edit($id){
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        return view('home.user.edit')->with(compact('stock'));
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'updated_by'    => 'required',
            'confirmation'  => 'required',
        ]);
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        $stock->updated_by = $request->updated_by;
        $stock->confirmation = $request->confirmation;
        $stock->save();
        $users = User::where('level', '=', 'admin')->get();
        try {
            foreach($users as $user){
                Mail::to($user->email)->send(new StockoutUpdated($stock));
            }
        } catch (\Exception $e) {
            return redirect()->route('user.home.index')
                ->with('error','Error when sending email.');
        }

        return redirect()->route('user.home.index')
            ->with('success','Stock Out updated successfully.');
    }
}
