<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockOpname;
use App\StockOpnameItem;
use App\Goody;
use App\Category;
use App\Unit;
use App\Building;

use Cart;
use Carbon\Carbon;
use PDF;

class StockOpnameController extends Controller
{
    public function index()
    {
        $stocks = StockOpname::all()->sortByDesc('created_at');
        return view('stockopname.index')->with(compact('stocks'));
    }
    
    public function create()
    {
        return view('stockopname.create');
    }

    public function summary()
    {
        if(Cart::isEmpty()){
            return redirect()->route('stockopname.create')
                ->with('error','Cart must be fill.');
        }
        return view('stockopname.summary');
    }

    public function checkId()
    {
        $stock = StockOpname::latest()->first();
        return response()->json(compact('stock'));
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'created_by'        => 'required',
            'id_transaction'    => 'required|unique:stock_opname',
            'name'              => 'required',
        ]);

        $date = Carbon::parse($request->date)->format('Y-m-d');//for date column

        $stock = new StockOpname;
        $stock->id_transaction = $request->id_transaction;
        $stock->name = $request->name;
        $stock->date = $date;
        $stock->description = $request->description;
        $stock->created_by = $request->created_by;
        $stock->save();

        $stock = StockOpname::where('id_transaction', '=', $request->id_transaction)->firstOrFail();//for get id in stock_opname
        $cartCollection = Cart::getContent();
        foreach ($cartCollection as $collection){
            $item = new StockOpnameItem;
            $item->stock_id = $stock->id;
            $item->goody_id = $collection->id;
            $item->qty = $collection->quantity;
            $item->description = $collection->attributes->description;
            $item->save();

            //update qty in goodies
            $goody = Goody::where('id', '=', $collection->id)->firstOrFail();
            $goody->qty = $collection->quantity;
            $goody->save();
        }
        Cart::clear();
        return redirect()->route('stockopname.index')
                ->with('Success','Stock In created Successfully.');
    }

    public function show($id){
        $stock = StockOpname::where('id', '=', $id)->firstOrFail();
        $items = StockOpnameItem::where('stock_id', '=', $stock->id)->get();
        return view('stockopname.show')->with(compact('stock', 'items'));
    }

    public function destroy($id)
    {
        $stock = StockOpname::where('id', '=', $id)->firstOrFail();
        $stock->delete();
        return redirect()->action('StockOpnameController@index')
            ->with('success','Stock deleted successfully.');
    }

    public function exportSinglePdf($id)
    {
        $stock = StockOpname::where('id', '=', $id)->firstOrFail();
        $building = Building::latest()->first();
        if(empty($building)){
            return redirect()->action('StockOpnameController@index')
                ->with('error','Fill Building Information first, ask Admin for further information.');
        }
        $name = "Stock Opname";
        $type = "stockopname";
        $pdf = PDF::loadview('pdf-single', compact('stock', 'name', 'type', 'building'))->setPaper('a4', 'potrait');
        $filename = 'Stock Opname - ' . $stock->id_transaction . '.pdf';
        return $pdf->download($filename);
    }

}
