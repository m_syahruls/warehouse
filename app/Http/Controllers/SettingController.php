<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;

use File;

class SettingController extends Controller
{
    public function index()
    {
        $building = Building::latest()->first();
        return view('setting')->with(compact('building'));
    }

    public function storeBuilding(Request $request)
    {
        $request->validate([
            'building_name' => 'required',
            'address'       => 'required',
            'district'      => 'required',
            'city'          => 'required',
            'province'      => 'required',
            'postal'        => 'required',
            'phone'         => 'required',
            'email'         => 'required',
            'website'       => 'required',
        ]);
        

        $image_name = $request->hidden_image;
        $image = $request->file('image');
        if($image != ''){
            $request->validate([
                'image'     => 'required|image|max:2048',
            ]);
            $image_path = "images/".$image_name;  // Value is not URL but directory file path
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $image_name = "logo." . $image->getClientOriginalExtension();
            $image->move(public_path('images/'), $image_name);
        }

        $building = new Building;
        $building->name = $request->building_name;
        $building->address = $request->address;
        $building->district = $request->district;
        $building->city = $request->city;
        $building->province = $request->province;
        $building->postal = $request->postal;
        $building->phone = $request->phone;
        $building->email = $request->email;
        $building->website = $request->website;
        $building->image = $image_name;
        $building->save();
        return redirect()->action('SettingController@index')
            ->with('success','Building updated successfully.');
    }
}
