<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('username', '=', auth()->user()->username)->firstOrFail();
        return view('profile.index')->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', '=', $id)->where('username', '=', auth()->user()->username)->firstOrFail();
        return view('profile.edit')->with(compact('user'));
    }

    public function editPassword($id)
    {
        $user = User::where('id', '=', $id)->where('username', '=', auth()->user()->username)->firstOrFail();
        return view('profile.edit-password')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();
        if($request->password){
            $validate = $request->validate([
                'old_password' => ['required'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            if (Hash::check($request->old_password, $user->password)) { 
                $user->fill(['password' => Hash::make($request->password)])->save();
                    return redirect()->action('ProfileController@index', 'u='.$user->username)
            ->with('success','Password changed.');
             } else {
                 return redirect()->action('ProfileController@editPassword', $user->id)
                    ->with('error','Password does not match.');
             }
            $user->password = $request->password;
            $user->save();
        }

        if($request->username != $user->username){
            $validate = $request->validate([
                'username' => ['required', 'string', 'max:255', 'unique:users', 'alpha_dash'],
            ]);
        }
        if($request->email != $user->email){
            $validate = $request->validate([
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]);
        }
        $validate = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'alpha_dash'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $photo_name = $request->hidden_photo;
        $photo = $request->file('photo');
        if($photo != ''){
            $request->validate([
                'photo'     => 'required|image|max:2048',
            ]);
            $photo_path = "images/users/".$photo_name;  // Value is not URL but directory file path
            if($photo_name != "default.png"){
                if(File::exists($photo_path)) {
                    File::delete($photo_path);
                }
            }
            $photo_name = rand() . '.' . $photo->getClientOriginalExtension();
            $photo->move(public_path('images/users'), $photo_name);
        }

        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->photo = $photo_name;
        $user->save();
        return redirect()->action('ProfileController@index')
            ->with('success','Profile updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
