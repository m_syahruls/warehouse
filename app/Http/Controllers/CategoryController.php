<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all()->sortBy('code');
        return view('category.index')->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'code' => 'required|unique:categories',
            'name' => 'required',
        ]);
        $category = new Category;
        $category->code = $request->code;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return redirect()->action('CategoryController@index')
            ->with('success','Category created successfully.');
    }

    public function storeStockin(Request $request)
    {
        $validate = $request->validate([
            'code' => 'required|unique:categories',
            'name' => 'required',
        ]);
        $category = new Category;
        $category->code = $request->code;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return response()->json($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('category.edit')->with(compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        if ($category->code != $request->code){
            $request->validate([
                'code' => 'required|unique:categories',
            ]);
        }
        $request->validate([
            'name' => 'required',
        ]);
        $category->update($request->all());
        return redirect()->action('CategoryController@index')
            ->with('success','Category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->action('CategoryController@index')
            ->with('success','Category deleted successfully.');
    }
}
