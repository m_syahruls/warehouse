<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockOut;
use App\StockOutItem;
use App\Goody;

use App\Mail\StockoutConfirmation;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $stocks = StockOut::where('confirmation', '=', 'unconfirmed')->orWhere('confirmation', '=', 'progress')->get()->sortByDesc('created_at');
        return view('home.index')->with(compact('stocks'));
    }

    public function show($id){
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        return view('home.show')->with(compact('stock'));
    }

    public function edit($id){
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        return view('home.edit')->with(compact('stock'));
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'updated_by'    => 'required',
            'confirmation'  => 'required',
        ]);
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        $stock->updated_by = $request->updated_by;
        $stock->confirmation = $request->confirmation;
        // update qty in goodies
        if($request->confirmation == 'done'){
            $items = StockOutItem::where('stock_id', '=', $id)->get();
            // checking amount
            $errors = [];
            foreach($items as $item){
                $goody = Goody::where('id', '=', $item->goody_id)->firstOrFail();
                if($goody->qty<$item->qty_confirm){
                    $errors[] = $goody->name." is out of stock, ".$goody->qty. " left in warehouse";
                }
            }
            // checking errors
            if (count($errors)>0){
                // if qty exceed stock, return back and notif error
                return redirect()->route('home.edit', $stock->id)
                    ->with('errors', $errors);
            } else {
                // stock reduced by qty
                foreach($items as $item){
                    $goody = Goody::where('id', '=', $item->goody_id)->firstOrFail();
                    $goody->qty -= $item->qty_confirm;
                    $goody->save();
                }
            }
        }
        $stock->save();
        try {
            Mail::to($stock->create_by->email)->send(new StockoutConfirmation($stock));
        } catch (\Exception $e) {
            return redirect()->route('home.index')
                ->with('error','Error when sending email.');
        }

        return redirect()->route('home.index')
            ->with('success','Stock Out updated successfully.');
    }

    public function destroy($id)
    {
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        $stock->delete();
        return redirect()->route('home.index')
            ->with('success','Stock Out deleted successfully.');
    }
}
