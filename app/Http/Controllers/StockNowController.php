<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Goody;
use App\Category;
use App\Unit;
use App\Building;

use PDF;
use File;
use Carbon\Carbon;

class StockNowController extends Controller
{
    public function index()
    {
        return view('stocknow.index');
    }

    public function show($id)
    {
        $goody = Goody::where('id', '=', $id)->firstOrFail();
        return view('stocknow.show')->with(compact('goody'));
    }

    public function exportPdf()
    {
        $goodies = Goody::all()->sortBy('code', SORT_NATURAL, false);
        $building = Building::latest()->first();
        if(empty($building)){
            return redirect()->action('StockNowController@index')
                ->with('error','Fill Building Information first, ask Admin for further information.');
        }
        $pdf = PDF::loadview('stocknow.pdf-all',compact('goodies', 'building'))->setPaper('a4', 'potrait');
        $filename = 'Stock Now - ' . Carbon::now()->format('d-m-Y H.i.s') . '.pdf';
        return $pdf->download($filename);
    }
}
