<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockOut;
use App\Building;

use PDF;

class StockOutController extends Controller
{
    public function index()
    {
        $stocks = StockOut::all()->sortByDesc('created_at');
        return view('stockout.index')->with(compact('stocks'));
    }

    public function show($id)
    {
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        foreach($stock->items as $item){
            $item->returned = 0;
            foreach($item->stockitem as $item_return){
                $item->returned += $item_return->qty;
            }
            if ($item->returned == $item->qty_confirm) {
                $item->returned = "All";
            }
        }
        return view('stockout.show')->with(compact('stock'));
    }

    // EDIT and UPDATE only in HomeController

    public function edit($id)
    {
        // 
    }

    public function update(Request $request, $id)
    {
        // 
    }

    public function destroy($id)
    {
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        $stock->delete();
        return redirect()->route('stockout.index')
            ->with('success','Stock Out deleted successfully.');
    }

    public function exportSinglePdf($id)
    {
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        $building = Building::latest()->first();
        if(empty($building)){
            return redirect()->action('TransactionController@index')
                ->with('error','Fill Building Information first, ask Admin for further information.');
        }
        $name = "Stock Out";
        $type = "stockout";
        $pdf = PDF::loadview('pdf-single', compact('stock', 'name', 'type', 'building'))->setPaper('a4', 'potrait');
        $filename = 'Stock Out - ' . $stock->id_transaction . '.pdf';
        return $pdf->download($filename);
    }
}
