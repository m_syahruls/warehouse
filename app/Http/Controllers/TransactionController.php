<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Goody;
use App\User;
use App\StockOut;
use App\StockOutItem;

use Carbon\Carbon;
use Cart;
use DataTables;

use App\Mail\StockoutCreated;
use Illuminate\Support\Facades\Mail;

class TransactionController extends Controller
{
    public function index()
    {
        return view('transaction.index');
    }
    
    public function create()
    {
        return view('transaction.create');
    }

    public function summary()
    {
        if(Cart::isEmpty()){
            return redirect()->route('transaction.create')
                ->with('error','Cart must be fill.');
        }
        return view('transaction.summary');
    }

    public function checkId()
    {
        $stock = StockOut::latest()->first();
        return response()->json(compact('stock'));
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'created_by'           => 'required',
            'id_transaction'       => 'required|unique:stock_out',
            // 'description'          => 'required',
        ]);

        $date = Carbon::parse($request->date)->format('Y-m-d');//for date column

        $stock = new StockOut;
        $stock->id_transaction = $request->id_transaction;
        $stock->date = $date;
        $stock->description = $request->description;
        $stock->created_by = $request->created_by;
        $stock->updated_by = $request->created_by;
        $stock->status = $request->status;
        $stock->confirmation = "unconfirmed";
        $stock->save();

        $stock = StockOut::where('id_transaction', '=', $request->id_transaction)->firstOrFail();//for get id in stock_out
        $cartCollection = Cart::getContent();
        foreach ($cartCollection as $collection){
            $item = new StockOutItem;
            $item->stock_id = $stock->id;
            $item->goody_id = $collection->id;
            $item->qty = $collection->quantity;
            $item->qty_confirm = $collection->quantity;
            $item->description = $collection->attributes->description;
            $item->save();
        }
        Cart::clear();

        // emailing all admin
        $users = User::where('level', '=', 'admin')->get();
        try {
            foreach($users as $user){
                Mail::to($user->email)->send(new StockoutCreated($stock));
            }
        } catch (\Exception $e) {
            return redirect()->route('transaction.index')
                ->with('error','Error when sending email.');
        }

        return redirect()->route('transaction.index')
                ->with('success','Stock Out created Successfully.');
    }

    public function show($id)
    {
        $stock = StockOut::where('id', '=', $id)->firstOrFail();
        foreach($stock->items as $item){
            $item->returned = 0;
            foreach($item->stockitem as $item_return){
                $item->returned += $item_return->qty;
            }
            if ($item->returned == $item->qty_confirm) {
                $item->returned = "All";
            }
        }
        return view('transaction.show')->with(compact('stock'));
    }

    // EDIT and UPDATE only in HomeController
    // DESTROY in HomeController and StockOutController

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        // 
    }

    public function destroy($id)
    {
        // 
    }
}
