<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Goody;
use App\StockOutItem;

use Cart;
use DataTables;

class CartController extends Controller
{
    public function index()
    {
        $cartCollection = Cart::getContent();

        $i=0;
        $goodies=[];
        foreach($cartCollection as $collection){
            $goodies[$i] = Goody::where('id', '=', $collection->id)->firstOrFail();
            $goodies[$i]->qty = $collection->quantity . " " . $goodies[$i]->unit->name;
            $goodies[$i]->description = $collection->attributes->description;
            $i++;
        }

        return Datatables::of($goodies)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return $this->getActionColumn($data);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function getActionColumn($data)
    {
        $btn = '<a href="#" class="btn btn-warning btn-circle btn-sm mb-1" data-toggle="modal" id="editCart" data-target="#addModal" data-id="'.$data->id.'">
                  <i class="fas fa-edit"></i></a> ';
        $btn = $btn.' <a href="#" class="btn btn-danger btn-circle btn-sm mb-1" data-toggle="modal" id="removeCart" data-target="#removeModal" data-id="'.$data->id.'">
                  <i class="fas fa-trash"></i></a>';
        return $btn;
    }

    public function list()
    {
        $goodies = Goody::orderBy('code')->get();
        foreach($goodies as $goody){
            $goody->qty_unit = $goody->qty." ".$goody->unit->name;
            $goody->code_category = $goody->category->code;
        }

        return Datatables::of($goodies)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return $this->getAddColumn($data);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function getAddColumn($data)
    {
        $btn = '<a href="#" class="btn btn-primary btn-circle btn-sm mb-1" data-toggle="modal" id="addGoody" data-target="#addModal" data-id="'.$data->id.'">
                  <i class="fas fa-plus"></i></a>';
        return $btn;
    }

    public function show($id)
    {
        $goody = Goody::where('id', '=', $id)->firstOrFail();
        return response()->json($goody);
    }

    public function getStock($id)
    {
        Cart::clear();
        $items = StockOutItem::where('stock_id', '=', $id)
        ->ordoesnthave('stockitem')->where('stock_id', '=', $id)
        ->get();

        foreach($items as $item){
            if ($item->stockitem){
                foreach($item->stockitem as $stockitem){
                    $item->qty_confirm-=$stockitem->qty;
                }
                if ($item->qty_confirm==0){
                    continue;
                }
            }
            Cart::add(array(
                'id'        => $item->goody->id,
                'name'      => $item->goody->code,
                'quantity'  => $item->qty_confirm,
                'price' => 0,
                'attributes' => array(
                    'description' => $item->description,
                )
            ));
        }
        return response()->json($items);
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'id'           => 'required',
            'code'         => 'required',
            'qty'          => 'required|numeric|min:0',
        ]);
        $cartCollection = Cart::getContent();
        $vali = true;
        foreach($cartCollection as $collection){
            if($collection->id == $request->id){
                $vali = false;
                $validate = "Goodies already in Cart";
                break;
            }
            $vali = true;
        }
        if($vali == true){
            Cart::add(array(
                'id'        => $request->id,
                'name'      => $request->code,
                'quantity'  => 1,
                'price'     => 0,
                'attributes' => array(
                    'description' => $request->description,
                )
            ));
            Cart::update($request->id, array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->qty,
                ),
            ));
        }
        return response()->json(['created'=>true]);
    }

    // storing dummy goodies for development purpose
    // public function storeDemo()
    // {
    //     $requests = Goody::paginate(250);
    //     $cartCollection = Cart::getContent();
    //     $vali = true;
    //     foreach($requests as $request){
    //         if($vali == true){
    //             Cart::add(array(
    //                 'id'        => $request->id,
    //                 'name'      => $request->code,
    //                 'quantity'  => 1,
    //                 'price'     => 0,
    //                 'attributes' => array(
    //                     'description' => $request->description,
    //                 )
    //             ));
    //             Cart::update($request->id, array(
    //                 'quantity' => array(
    //                     'relative' => false,
    //                     'value' => $request->qty,
    //                 ),
    //             ));
    //         }
    //     }
    //     return response()->json("Demo Created");
    // }

    public function edit($id)
    {
        $getCart = Cart::get($id);
        $item = Goody::where('id', '=', $getCart->id)->firstOrFail();
        $item->cart_qty = $getCart->quantity;
        $item->cart_description = $getCart->attributes->description;
        return response()->json($item);
    }

    public function update(Request $request)
    {
        $validate = $request->validate([
            'id'           => 'required',
            'code'         => 'required',
            'qty'          => 'required|numeric|min:0',
        ]);

        Cart::update($request->id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->qty,
            ),
            'attributes' => array(
                'description' => $request->description,
            )
        ));
        return response()->json(['updated'=>true]);
    }

    public function clear()
    {
        Cart::clear();
        return response()->json(['cleared'=>true]);
    }

    public function destroy($id)
    {
        Cart::remove($id);
        return response()->json(['deleted'=>true]);
    }
}
