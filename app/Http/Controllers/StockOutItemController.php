<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockOutItem;

use DataTables;

class StockOutItemController extends Controller
{
    public function show($id)
    {
        $items = StockOutItem::where('stock_id', '=', $id)->get();
        foreach($items as $item){
            $item->name = $item->goody->name;
            $item->code = $item->goody->code;
            $item->unit = $item->goody->unit->name;
            $item->stock = $item->goody->qty;
        }

        return Datatables::of($items)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return $this->getEditColumn($data);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function getEditColumn($data)
    {
        $btn = '<a href="#" class="btn btn-warning btn-circle btn-sm mb-1" data-toggle="modal" id="editItem" data-target="#editModal" data-id="'.$data->id.'">
                  <i class="fas fa-edit"></i></a>';
        return $btn;
    }

    public function edit($id)
    {
        $item = StockOutItem::where('id', '=', $id)->firstOrFail();
        $item->name = $item->goody->name;
        $item->code = $item->goody->code;
        return response()->json($item);
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'id'           => 'required',
            'qty'          => 'required|numeric|min:0',
        ]);
        $item = StockOutItem::where('id', '=', $id)->firstOrFail();
        $item->qty_confirm = $request->qty;
        if($request->by=="user"){
            $item->qty = $request->qty;
        }
        $item->description = $request->description;
        $item->save();
        return response()->json(['updated'=>true]);
        // return response()->json($item);
    }
}
