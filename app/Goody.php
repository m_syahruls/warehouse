<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goody extends Model
{
    protected $fillable = [
        'category_id', 'code', 'name', 'photo', 'qty', 'unit_id', 'location', 'status', 'description'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function unit()
    {
    	return $this->belongsTo('App\Unit', 'unit_id', 'id');
    }
}
